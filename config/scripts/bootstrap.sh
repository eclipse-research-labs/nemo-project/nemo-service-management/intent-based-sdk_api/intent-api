#!/bin/bash

helm plugin install https://github.com/hypnoglow/helm-s3.git
helm s3 init --ignore-if-exists ${HELM_REPOSITORY_BUCKET}
helm repo add ${HELM_REPOSITORY_NAME} ${HELM_REPOSITORY_BUCKET}
#chown -R 1001:1001 /opt/app/uploads
#chown -R 1001:1001 /root/.config/helm
exec "$@"
