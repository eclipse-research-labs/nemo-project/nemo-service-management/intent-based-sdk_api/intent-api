from django.urls import path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from api import views as api_views

description = """
<h3>Login</h3>

<ol>
<li>To login via django, use the <code>/auth/login/</code>  endpoint and the click <code>Authorize</code>and enter <code>Token &lt;token_value&gt;</code>.</li>
<li>To login with keycloak jwt token, click <code>Authorize</code> and enter <code>Bearer &lt;jwt_token_value&gt;</code>.</li>
</ol>

<h3>Proxies</h3>
<ol>
<li>MOCA API is proxied at <code>/moca</code>. Swagger docs can be found at <code>/moca/api/v1/swagger</code>.</li>
</ol>
"""

schema_view = get_schema_view(
   openapi.Info(
      title="NEMO Intent API",
      default_version='v1',
      description=description,
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="tomaras@synelixis.com"),
      license=openapi.License(name="Apache License", url="https://www.apache.org/licenses/LICENSE-2.0.txt"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('swagger<format>/', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),

    # Auth views
    path('auth/login/', api_views.LoginView.as_view(), name='login'),
    path('auth/logout/', api_views.LogoutView.as_view(), name='logout'),

    # Intent views
    path('intent/', api_views.IntentListCreateView.as_view(), name='intent_list_create'),
    path('intent/types/', api_views.IntentTypeListView.as_view(), name='intent_types_list'),
    path('intent/template/', api_views.IntentTemplateView.as_view(), name='intent_template'),
    path('intent/<int:pk>/action/', api_views.IntentActionView.as_view(), name='intent_action'),
    path('intent/<int:pk>/target/', api_views.IntentTargetUpdateView.as_view(), name='intent_target_update'),


    # Workload views
    path('workload/upload/', api_views.WorkloadUploadView.as_view(), name='workload_upload'),
    path('workload/instance/', api_views.WorkloadDocumentInstanceView.as_view(), name='workload_instance'),
    path('workload/instance/<uuid:instance_id>/manifests/', api_views.WorkloadDocumentInstanceManifestsView.as_view(), name='workload_instance_manifests'),
    path('workload/instance/<uuid:instance_id>/delete/', api_views.WorkloadDocumentInstanceDeleteView.as_view(), name='workload_instance_delete'),
    path('workload/', api_views.WorkloadListCreateView.as_view(), name='workload_list_create'),
    path('workload/<int:pk>/', api_views.WorkloadDocumentRetrieveUpdateDestroyView.as_view(),
         name='workload_retrieve_update_delete'),
    path('workload/<int:pk>/template/', api_views.WorkloadDocumentTemplateView.as_view(),
         name='workload_document_template'),

    # Cluster views
    path('cluster/retrieve/', api_views.ClusterListView.as_view(), name='cluster_list'),
    path('cluster/retrieve/<uuid:id>/', api_views.ClusterRetrieveView.as_view(), name='cluster_retrieve'),
    path('cluster/register/', api_views.ClusterRegisterView.as_view(), name='cluster_register')
]