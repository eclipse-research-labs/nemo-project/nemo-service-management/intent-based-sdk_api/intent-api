from typing import IO, Any, Optional, Mapping, Dict

from drf_yaml.parsers import YAMLParser

from api.utils import transform_dict_keys


class CamelCaseYAMLParser(YAMLParser):
    def parse(
        self,
        stream: IO[Any],
        media_type: Optional[str] = None,
        parser_context: Optional[Mapping[str, Any]] = None,
    ) -> Dict[str, Any]:
        parsed_data = super().parse(stream, media_type, parser_context)
        return transform_dict_keys(parsed_data)