import yaml
from rest_framework.exceptions import ErrorDetail
from rest_framework.renderers import BaseRenderer


class YamlListRenderer(BaseRenderer):
    media_type = 'application/yaml'
    format = 'yaml'

    def render(self, data, media_type=None, renderer_context=None):
        if isinstance(data, list):
            # Check if the list contains ErrorDetail instances
            if all(isinstance(item, ErrorDetail) for item in data):
                # Error response, serialize each error detail in the list
                yaml_data = ''.join([yaml.dump({'error': item}, default_flow_style=False) + '---\n' for item in data])
            else:
                # Successful response, serialize each item in the list
                yaml_data = ''.join([yaml.dump(item, default_flow_style=False) + '---\n' for item in data])
        else:
            # Handle other cases as needed
            yaml_data = yaml.dump(data, default_flow_style=False)

        return yaml_data.encode(self.charset)
