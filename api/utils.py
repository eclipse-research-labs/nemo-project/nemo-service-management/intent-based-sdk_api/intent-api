import base64
import json
import logging
import re
import shutil
from copy import deepcopy
from datetime import datetime
from functools import reduce
from pathlib import Path
from typing import NamedTuple, List, Optional
from xmlrpc.client import Boolean

import sh
from django.db.models import Prefetch
from rest_framework import serializers

from api.serializers.intent import TimeWindowSerializer
from intent.models import ValueRangeType, IntentReport, ExpectationFulfilmentResult, TargetFulfilmentResult, \
    IntentExpectation, ExpectationTarget, TargetValueRange, Context, ContextValueRange
from nemo.utils import parse_cpu_quota, convert_to_milli_cpus, parse_memory_quota, convert_to_bytes

LOGGER = logging.getLogger(__name__)


def delete_directory(folder_path: Path) -> None:
    try:
        shutil.rmtree(folder_path)
    except OSError as e:
        LOGGER.error(f'Exception while deleting directory : {str(e)}')


def decode_docker_config_json(encoded_data):
    decoded_data = base64.b64decode(encoded_data).decode('utf-8')
    return json.loads(decoded_data)


def decode_docker_config_auth_data(encoded_data):
    return base64.b64decode(encoded_data).decode('utf-8').split(':')


def resolve_container_image(container_image):
    """
    Examples:
        busybox
        ealen/echo-server
        dockerhub.synelixis.com/library/adeies:v3.0.1
        eaegovgr/eae-backend-2023:v2.4.72
    """
    matched_repository_name = None
    matched_registry_name = None

    match container_image.split('/'):
        case [image_name_complex]:
            pass
        case [repository_name, image_name_complex]:
            matched_repository_name = repository_name
        case [registry_name, repository_name, image_name_complex]:
            matched_registry_name = registry_name
            matched_repository_name = repository_name
        case _:
            raise ValueError

    match image_name_complex.split(':'):
        case [image_name]:
            matched_image_name = image_name
            matched_image_tag = 'latest'
        case [image_name, image_tag]:
            matched_image_name = image_name
            matched_image_tag = image_tag
        case _:
            raise ValueError

    if not matched_registry_name:
        matched_registry_name = 'docker.io'

    return matched_registry_name, matched_repository_name, matched_image_name, matched_image_tag


def isolate_substring(input_string):
    match = re.search(r'msg\s*=\s*["\'](.*?)["\']', input_string)
    if match:
        return match.group(1)
    return ''


class SkopeoListTags(NamedTuple):
    error: bool = False
    unauthorized: bool = False
    reason: Optional[str] = None
    tags: Optional[List[str]] = None


def skopeo_list_tags(container_uri, username: str = None, password: str = None) -> SkopeoListTags:
    command_args = ['list-tags', container_uri]

    if username and password:
        command_args += [{'username': username}, {'password': password}]

    try:
        output = json.loads(sh.skopeo(*command_args))
        return SkopeoListTags(tags=output['Tags'])
    except sh.ErrorReturnCode as e:
        err_msg = e.stderr.lower().decode('utf-8')
        if 'authentication required' in err_msg or 'requested access to the resource is denied' in err_msg:
            return SkopeoListTags(error=True, unauthorized=True, reason=isolate_substring(err_msg))
        return SkopeoListTags(error=True, unauthorized=False, reason=isolate_substring(err_msg))


def collect_workload_resources(container_resources_mapping):
    mapping = deepcopy(container_resources_mapping)
    mapping['_total'] = {
        'requests': {'cpu': 0, 'memory': 0},
        'limits': {'cpu': 0, 'memory': 0}
    }

    for container_name, container_resources in container_resources_mapping.items():
        mapping['_total']['requests']['cpu'] += \
            reduce(convert_to_milli_cpus, *parse_cpu_quota(container_resources['requests']['cpu']))
        mapping['_total']['limits']['cpu'] += \
            reduce(convert_to_milli_cpus, *parse_cpu_quota(container_resources['limits']['cpu']))
        mapping['_total']['requests']['memory'] += \
            reduce(convert_to_bytes, *parse_memory_quota(container_resources['requests']['memory']))
        mapping['_total']['limits']['memory'] += \
            reduce(convert_to_bytes, *parse_memory_quota(container_resources['limits']['memory']))

    return mapping


def camel_to_snake(name):
    """
    Convert CamelCase to snake_case.
    """
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def transform_dict_keys(d):
    """
    Recursively transform dictionary keys from CamelCase to snake_case.
    """
    if isinstance(d, dict):
        new_dict = {}
        for key, value in d.items():
            new_key = camel_to_snake(key)
            new_dict[new_key] = transform_dict_keys(value)
        return new_dict
    elif isinstance(d, list):
        return [transform_dict_keys(item) for item in d]
    else:
        return d


def datetime_value_range_checker(value_range_data, case='context'):
    """Check for DateTime ValueRange

    contextValueRange:
        - startTime: '2023-10-27-22-00-00'
        - endTime: '2023-10-28-22-00-00'

    """
    is_valid = True
    reason = None
    start_time = None
    end_time = None
    total_keys = 0

    for value_range_input in value_range_data:
        keys = list(value_range_input.keys())
        keys_length = len(keys)
        if keys_length != 1:
            is_valid = False
            reason = 'Invalid DateTime {}ValueRange'.format(case)
        key = keys[0]
        if key == 'start_time':
            start_time = value_range_input[key]
            total_keys += 1
        if key == 'end_time':
            end_time = value_range_input[key]
            total_keys += 1

    match total_keys:
        case 1:
            if start_time or end_time:
                serializer = TimeWindowSerializer(data={'start_time': start_time, 'end_time': end_time})
                if serializer.is_valid():
                    pass
                else:
                    is_valid = False
                    reason = serializer.errors
            else:
                is_valid = False
                reason = 'DateTime {}ValueRange must have startTime or endTime'.format(case)
        case 2:
            if start_time and end_time:
                serializer = TimeWindowSerializer(data={'start_time': start_time, 'end_time': end_time})
                if serializer.is_valid():
                    pass
                else:
                    is_valid = False
                    reason = serializer.errors
            else:
                is_valid = False
                reason = 'DateTime {}ValueRange must have startTime & endTime'.format(case)
        case _:
            is_valid = False
            reason = 'Invalid DateTime {}ValueRange'.format(case)

    return is_valid, reason


def value_range_serializer(value_range_data, case='context'):
    """Serializer different types of valueRanges

    Raises:
        serializers.ValidationError
    """

    match value_range_data:
        case list():
            # Check for datetime
            is_valid, reason = datetime_value_range_checker(value_range_data, case=case)
            if not is_valid:
                raise serializers.ValidationError(reason)

            selected_type = ValueRangeType.SelectedType.TYPE_TIME_WINDOW
        case str():
            selected_type = ValueRangeType.SelectedType.TYPE_STRING
        case bool():
            selected_type = ValueRangeType.SelectedType.TYPE_BOOLEAN
        case int():
            selected_type = ValueRangeType.SelectedType.TYPE_INTEGER
        case float():
            selected_type = ValueRangeType.SelectedType.TYPE_NUMBER
        case datetime():
            selected_type = ValueRangeType.SelectedType.TYPE_DATETIME
        case _:
            raise serializers.ValidationError('Invalid intent {}ValueRange'.format(case))
    return selected_type


intent_full_prefetch = [
    Prefetch(
        'intent_contexts',
        queryset=Context.objects.prefetch_related(
                Prefetch(
                    'context_value_ranges',
                    queryset=ContextValueRange.objects.select_related('type_time_window')
                )
            )
    ),
    Prefetch(
        'intent_reports',
        queryset=IntentReport.objects.select_related(
            'intent_fulfilment_report__intent_fulfilment_info',
            'intent_feasibility_check_report'
        ).prefetch_related(
            Prefetch(
                'intent_fulfilment_report__expectation_fulfilment_results',
                queryset=ExpectationFulfilmentResult.objects.select_related(
                    'expectation_fulfilment_info').prefetch_related(
                    Prefetch(
                        'target_fulfilment_results',
                        queryset=TargetFulfilmentResult.objects.select_related('target_fulfilment_info')
                    )
                )
            )
        )
    ),
    Prefetch(
        'intent_expectations',
        queryset=IntentExpectation.objects.select_related('expectation_object').prefetch_related(
            Prefetch(
                'expectation_contexts',
                queryset=Context.objects.prefetch_related(
                    Prefetch(
                        'context_value_ranges',
                        queryset=ContextValueRange.objects.select_related('type_time_window')
                    )
                )
            ),
            Prefetch(
                'expectation_targets',
                queryset=ExpectationTarget.objects.prefetch_related(
                    Prefetch(
                        'target_value_ranges',
                        queryset=TargetValueRange.objects.select_related('type_time_window')
                    )
                )
            ),
            Prefetch(
                'expectation_object__object_contexts',
                queryset=Context.objects.prefetch_related(
                    Prefetch(
                        'context_value_ranges',
                        queryset=ContextValueRange.objects.select_related('type_time_window')
                    )
                )
            )
        )
    )
]