from datetime import datetime

from django.db import transaction
from rest_framework import serializers
from rest_framework.fields import SerializerMethodField

from api.fields import TargetValueRangeField, ContextValueRangeField
from intent.models import Intent, IntentExpectation, ValueRangeType, TimeWindow, ExpectationObject, Context, \
    ContextValueRange, ExpectationTarget, TargetValueRange, IntentReport, IntentFulfilmentReport, FulfilmentInfo, \
    FulfilmentStatus, NotFulfilledState, IntentFeasibilityReport, ExpectationFulfilmentResult, TargetFulfilmentResult, \
    Condition, IntentActionChoices
from intent.tasks import intent_feasibility_check
from intent.utils import create_context, create_target
from nemo.constants import IntentUserLabels
from nemo.models.workload import WorkloadDocumentInstance


class TimeWindowSerializer(serializers.ModelSerializer):
    class Meta:
        model = TimeWindow
        fields = ('start_time', 'end_time')

    def validate(self, attrs):
        start_time = attrs.get('start_time', None)
        end_time = attrs.get('end_time', None)

        if start_time and end_time and end_time < start_time:
            raise serializers.ValidationError('End time must be greater than start time')

        return attrs


class ContextInputSerializer(serializers.ModelSerializer):
    context_value_range = ContextValueRangeField()

    class Meta:
        model = Context
        fields = ('context_attribute', 'context_condition', 'context_value_range')


class ExpectationTargetInputSerializer(serializers.ModelSerializer):
    target_value_range = TargetValueRangeField()
    target_contexts = ContextInputSerializer(many=True, required=False)

    class Meta:
        model = ExpectationTarget
        fields = ('target_name', 'target_condition', 'target_value_range', 'target_contexts')


class ExpectationObjectInputSerializer(serializers.ModelSerializer):
    object_contexts = ContextInputSerializer(many=True, required=False)

    class Meta:
        model = ExpectationObject
        fields = ('object_type', 'object_instance', 'context_selectivity', 'object_contexts')

    def validate(self, attrs):
        if attrs['object_type'] == ExpectationObject.ObjectType.NEMO_WORKLOAD and not  \
                WorkloadDocumentInstance.objects.filter(instance_id=attrs['object_instance']).exists():
            raise serializers.ValidationError('Object instance does not exist for NEMO_WORKLOAD')
        return attrs


class IntentExpectationInputSerializer(serializers.ModelSerializer):
    expectation_object = ExpectationObjectInputSerializer()
    expectation_targets = ExpectationTargetInputSerializer(many=True)
    expectation_contexts = ContextInputSerializer(many=True, required=False)

    class Meta:
        model = IntentExpectation
        fields = ('expectation_id', 'expectation_verb', 'expectation_object', 'expectation_targets',
                  'expectation_contexts')


class IntentInputSerializer(serializers.ModelSerializer):
    intent_expectations = IntentExpectationInputSerializer(many=True)
    intent_contexts = ContextInputSerializer(many=True, required=False)

    class Meta:
        model = Intent
        fields = ('user_label', 'context_selectivity', 'intent_preemption_capability', 'observation_period',
                  'intent_expectations', 'intent_contexts')
        read_only_fields = ('id',)


class IntentInputAttributeSerializer(serializers.Serializer):
    intent = IntentInputSerializer()

    def create(self, validated_data):
        intent_data = validated_data['intent']

        with transaction.atomic():
            intent_expectations = intent_data.pop('intent_expectations')
            intent_contexts_data = intent_data.pop('intent_contexts', [])

            intent = Intent.objects.create(
                **intent_data
            )

            for intent_context_data in intent_contexts_data:
                intent_context = create_context(intent, intent_context_data)

            for intent_expectation_data in intent_expectations:
                expectation_contexts_data = intent_expectation_data.pop('expectation_contexts', [])
                expectation_object_data = intent_expectation_data.pop('expectation_object')
                expectation_targets_data = intent_expectation_data.pop('expectation_targets')
                object_contexts_data = expectation_object_data.pop('object_contexts', [])

                expectation_object = ExpectationObject.objects.create(
                    **expectation_object_data
                )

                intent_expectation = IntentExpectation.objects.create(
                    intent=intent,
                    expectation_object=expectation_object,
                    **intent_expectation_data
                )

                for expectation_context_data in expectation_contexts_data:
                    expectation_context = create_context(intent_expectation, expectation_context_data)

                for expectation_target_data in expectation_targets_data:
                    expectation_target = create_target(intent_expectation, expectation_target_data)

                for object_context_data in object_contexts_data:
                    object_context = create_context(expectation_object, object_context_data)

            # Initialize Intent Report
            intent_fulfilment_info = FulfilmentInfo.objects.create(
                fulfilment_status=FulfilmentStatus.NOT_FULFILLED,
                not_fulfilled_state=NotFulfilledState.ACKNOWLEDGED,
            )

            intent_fulfilment_report = IntentFulfilmentReport.objects.create(
                intent_fulfilment_info=intent_fulfilment_info
            )

            IntentReport.objects.create(
                intent_reference=intent,
                intent_fulfilment_report=intent_fulfilment_report
            )

        # Run Feasibility Check
        intent_feasibility_check.delay(intent.pk)

        return intent

####


class IntentOutputSerializer(serializers.Serializer):
    intent = serializers.IntegerField(help_text='Intent ID (PK)')


class ContextSerializer(serializers.ModelSerializer):
    context_value_range = ContextValueRangeField(source='context_value_ranges')

    class Meta:
        model = Context
        fields = ('id', 'context_attribute', 'context_condition', 'context_value_range')
        read_only_fields = ('id',)


class ExpectationTargetSerializer(serializers.ModelSerializer):
    target_value_range = TargetValueRangeField(source='target_value_ranges')
    target_contexts = ContextSerializer(many=True)

    class Meta:
        model = ExpectationTarget
        fields = ('id', 'target_name', 'target_condition', 'target_value_range', 'target_contexts')
        read_only_fields = ('id',)


class ExpectationObjectSerializer(serializers.ModelSerializer):
    object_contexts = ContextSerializer(many=True)

    class Meta:
        model = ExpectationObject
        fields = ('id', 'object_type', 'object_instance', 'context_selectivity', 'object_contexts')
        read_only_fields = ('id',)


class IntentExpectationSerializer(serializers.ModelSerializer):
    expectation_object = ExpectationObjectSerializer()
    expectation_targets = ExpectationTargetSerializer(many=True)
    expectation_contexts = ContextSerializer(many=True)

    class Meta:
        model = IntentExpectation
        fields = ('id', 'expectation_id', 'expectation_verb', 'expectation_object', 'expectation_targets', 'expectation_contexts')
        read_only_fields = ('id',)


class IntentFeasibilityReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = IntentFeasibilityReport
        fields = ('id', 'feasibility_check_type', 'infeasibility_reason')


class FulfilmentInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FulfilmentInfo
        fields = ('fulfilment_status', 'not_fulfilled_state', 'not_fulfilled_reasons')


class TargetFulfilmentResult(serializers.ModelSerializer):
    target_fulfilment_info = FulfilmentInfoSerializer()
    target = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = TargetFulfilmentResult
        fields = ('target', 'target_achieved_value', 'target_fulfilment_info')


class ExpectationFulfilmentResultSerializer(serializers.ModelSerializer):
    expectation_fulfilment_info = FulfilmentInfoSerializer()
    expectation_id = serializers.PrimaryKeyRelatedField(source='expectation', read_only=True)
    target_fulfilment_results = TargetFulfilmentResult(many=True)

    class Meta:
        model = ExpectationFulfilmentResult
        fields = ('expectation_fulfilment_info', 'expectation_id', 'target_fulfilment_results')


class IntentFulfilmentReportSerializer(serializers.ModelSerializer):
    intent_fulfilment_info = FulfilmentInfoSerializer()
    expectation_fulfilment_results = ExpectationFulfilmentResultSerializer(many=True)

    class Meta:
        model = IntentFulfilmentReport
        fields = ('id', 'intent_fulfilment_info', 'expectation_fulfilment_results')


class IntentReportSerializer(serializers.ModelSerializer):
    intent_feasibility_check_report = IntentFeasibilityReportSerializer()
    intent_fulfilment_report = IntentFulfilmentReportSerializer()

    class Meta:
        model = IntentReport
        fields = ('id', 'intent_fulfilment_report', 'intent_feasibility_check_report', 'last_updated_time')


class IntentSerializer(serializers.ModelSerializer):
    intent_expectations = IntentExpectationSerializer(many=True)
    intent_report_reference = SerializerMethodField()
    intent_contexts = ContextSerializer(many=True)

    class Meta:
        model = Intent
        fields = ('id', 'user_label', 'intent_preemption_capability', 'observation_period',
                  'intent_expectations', 'intent_report_reference', 'intent_contexts')
        read_only_fields = ('id',)

    def get_intent_report_reference(self, obj):
        return IntentReportSerializer(obj.intent_reports.all()[0]).data


class IntentTypeSerializer(serializers.Serializer):
    types = serializers.ChoiceField(choices=IntentUserLabels.values, help_text='Intent userLabels')


class TargetTemplateSerializer(serializers.Serializer):
    target_name = serializers.CharField(max_length=50)
    target_condition = serializers.ChoiceField(choices=Condition)
    target_value_range = TargetValueRangeField()

    def validate(self, attrs):
        intent_type = self.context.get('intent_type', None)

        if intent_type:
            # Add your validation logic here
            match intent_type:
                case IntentUserLabels.DELIVER_COMPUTING_WORKLOAD:
                    if attrs['target_name'] not in ['cpuUsage', 'ramUsage', 'diskUsage', 'rate']:
                        raise serializers.ValidationError('Invalid target_name={} for intent_type={}'.format(
                            attrs['target_name'], intent_type
                        ))
                case IntentUserLabels.ENERGY_CARBON_EFFICIENCY:
                    if attrs['target_name'] not in ['compEnergyEfficiency', 'compEnergyConsumption', 'greenEnergyConsumptionRate']:
                        raise serializers.ValidationError('Invalid target_name={} for intent_type={}'.format(
                            attrs['target_name'], intent_type
                        ))
                case IntentUserLabels.FEDERATED_LEARNING | IntentUserLabels.SECURE_EXECUTION:
                    if attrs['target_name'] != 'clusterType':
                        raise serializers.ValidationError('Invalid target_name={} for intent_type={}'.format(
                            attrs['target_name'], intent_type
                        ))
                case IntentUserLabels.AVAILABILITY:
                    if attrs['target_name'] != 'availability':
                        raise serializers.ValidationError('Invalid target_name={} for intent_type={}'.format(
                            attrs['target_name'], intent_type
                        ))
        return attrs

class IntentTemplateSerializerMixin(metaclass=serializers.SerializerMetaclass):
    intent_type = serializers.ChoiceField(choices=IntentUserLabels.values, help_text='Intent userLabels')
    service_start_time = serializers.DateTimeField(required=False, help_text='Optional')
    service_end_time = serializers.DateTimeField(required=False, help_text='Optional')
    targets = TargetTemplateSerializer(many=True, help_text='Expectation targets')

    def to_internal_value(self, data):
        # Extract the intent_type from the incoming data
        intent_type = data.get('intent_type')

        # Pass intent_type to each TargetTemplateSerializer
        self.fields['targets'].context.update({'intent_type': intent_type})

        # Continue with the default validation
        ret = super().to_internal_value(data)

        return ret

    def get_workload_document(self, attrs=None):
        raise NotImplementedError

    def get_instance_id(self, validated_data, instance_id=None):
        raise NotImplementedError

    def validate(self, attrs):
        workload_document = self.get_workload_document(attrs)

        if attrs['intent_type'] not in workload_document.intents:
            raise serializers.ValidationError('Invalid intent_type={} for workload_document={}'.format(
                attrs['intent_type'], str(workload_document))
            )

        dates = (attrs.get('service_start_time', None), attrs.get('service_end_time', None))
        match dates:
            case (datetime(), datetime()):
                pass
            case (None, None):
                pass
            case (datetime(), None) | (None, datetime()):
                raise serializers.ValidationError('Must specify both service_start_time and service_end_time')

        return attrs

    def create(self, validated_data, instance_id=None):
        object_instance = self.get_instance_id(validated_data, instance_id)
        object_contexts = []
        service_start_time = validated_data.get('service_start_time', None)
        service_end_time = validated_data.get('service_end_time', None)

        if service_start_time and service_end_time:
            object_contexts = [
                {
                    'context_attribute': 'serviceStartTime',
                    'context_condition': 'IS_EQUAL_TO',
                    'context_value_range': service_start_time,
                },
                {
                    'context_attribute': 'serviceEndTime',
                    'context_condition': 'IS_EQUAL_TO',
                    'context_value_range': service_end_time,
                }
            ]

        match validated_data['intent_type']:
            case IntentUserLabels.DELIVER_COMPUTING_WORKLOAD | IntentUserLabels.ENERGY_CARBON_EFFICIENCY | IntentUserLabels.AVAILABILITY:
                expectation_verb = 'ENSURE'
            case IntentUserLabels.FEDERATED_LEARNING | IntentUserLabels.SECURE_EXECUTION:
                expectation_verb = 'DELIVER'
            case _:
                raise ValueError('Invalid intent_type={}'.format(validated_data['intent_type']))

        targets = []
        for target in validated_data['targets']:
            targets.append({
                'target_name': target['target_name'],
                'target_condition': target['target_condition'],
                'target_value_range': target['target_value_range']['data'],
            })

        intent_obj = {
            'intent': {
                'user_label': validated_data['intent_type'],
                'intent_expectations': [
                    {
                        'expectation_id': '1',
                        'expectation_verb': expectation_verb,
                        'expectation_object': {
                            'object_instance': str(object_instance),
                            'object_type': 'NEMO_WORKLOAD',
                            'object_contexts': object_contexts
                        },
                        'expectation_targets': targets
                    }
                ],
                'intent_priority': 1,
                'observation_period': 60,
                'intent_admin_state': 'ACTIVATED'
            }
        }

        intent_serializer = IntentInputAttributeSerializer(data=intent_obj)
        intent_serializer.is_valid(raise_exception=True)
        intent_validated_data = intent_serializer.validated_data
        intent = intent_serializer.create(intent_validated_data)

        return intent.pk


class IntentTemplateEmbeddedSerializer(IntentTemplateSerializerMixin, serializers.Serializer):
    def get_workload_document(self, attrs=None):
        return self.context['workload_document']

    def get_instance_id(self, validated_data, instance_id=None):
        return instance_id

class IntentTemplateSerializer(IntentTemplateSerializerMixin, serializers.Serializer):
    instance_id = serializers.UUIDField(help_text='NEMO Workload instance ID')

    def validate_instance_id(self, instance_id):
        if not WorkloadDocumentInstance.objects.scope(self.context['request'].user).filter(instance_id=instance_id).exists():
            raise serializers.ValidationError('Invalid instance_id={}'.format(instance_id))
        return instance_id

    def get_workload_document(self, attrs=None):
        workload_document_instance = WorkloadDocumentInstance.objects.scope(self.context['request'].user).select_related(
            'workload_document'
        ).get(instance_id=attrs['instance_id'])

        return workload_document_instance.workload_document

    def get_instance_id(self, validated_data, instance_id=None):
        return validated_data['instance_id']

class IntentTemplateInputSerializer(serializers.Serializer):
    intents = IntentTemplateSerializer(many=True)


class IntentActionInputSerializer(serializers.Serializer):
    action = serializers.ChoiceField(choices=IntentActionChoices, help_text='Action to perform')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.intent = self.instance

        try:
            intent_report_qs = self.intent.intent_reports.all()
        except AttributeError:
            return

        self.intent_report = intent_report_qs[0]
        self.intent_fulfilment_report = self.intent_report.intent_fulfilment_report
        self.intent_fulfilment_info = self.intent_fulfilment_report.intent_fulfilment_info

    def validate(self, attrs):
        action = attrs.get('action')

        not_fulfilled_state = self.intent_fulfilment_info.not_fulfilled_state

        match not_fulfilled_state:
            case [NotFulfilledState.FULFILMENTFAILED, NotFulfilledState.TERMINATED, NotFulfilledState.ACKNOWLEDGED]:
                raise serializers.ValidationError(
                    'Cannot perform action={} on intent={}. Intent is in state={}'.format(
                        action, self.intent.pk, not_fulfilled_state
                    ))

            case _:
                match (not_fulfilled_state, action):
                    case (_, IntentActionChoices.TERMINATE):
                        pass
                    case (NotFulfilledState.SUSPENDED, IntentActionChoices.RESUME):
                        pass
                    case (NotFulfilledState.COMPLIANT, IntentActionChoices.SUSPEND):
                        pass
                    case (NotFulfilledState.DEGRADED, IntentActionChoices.SUSPEND):
                        pass
                    case _:
                        raise serializers.ValidationError(
                            'Cannot perform action={} on intent={}. Intent is in state={}'.format(
                                action, self.intent.pk, not_fulfilled_state
                            ))
        return attrs

    def update(self, instance, validated_data):
        action = validated_data.get('action')

        match action:
            case IntentActionChoices.RESUME:
                new_not_fulfilled_state = NotFulfilledState.COMPLIANT
            case IntentActionChoices.SUSPEND:
                new_not_fulfilled_state = NotFulfilledState.SUSPENDED
            case IntentActionChoices.TERMINATE:
                new_not_fulfilled_state = NotFulfilledState.TERMINATED
            case _:
                raise ValueError

        self.intent_fulfilment_info.not_fulfilled_state = new_not_fulfilled_state
        self.intent_fulfilment_info.save()

        return instance


class IntentTargetUpdateSerializer(serializers.Serializer):
    target_id = serializers.IntegerField(min_value=1, help_text='Target ID')
    target_value_range = TargetValueRangeField(help_text='New target value')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.intent = self.instance

        try:
            intent_report_qs = self.intent.intent_reports.all()
            intent_expectation_qs = self.intent.intent_expectations.all()
        except AttributeError:
            return

        self.intent_report = intent_report_qs[0]
        self.intent_fulfilment_report = self.intent_report.intent_fulfilment_report
        self.intent_fulfilment_info = self.intent_fulfilment_report.intent_fulfilment_info

        self.intent_expectation = intent_expectation_qs[0]
        self.expectation_targets = self.intent_expectation.expectation_targets.all()
        self.expectation_targets_mapping = {expectation_target.pk: expectation_target for expectation_target in
                                            self.expectation_targets}

    def validate(self, attrs):
        target_id = attrs.get('target_id')

        if target_id not in self.expectation_targets_mapping.keys():
            raise serializers.ValidationError('Invalid expectation_target_id={} for intent={}'.format(
                target_id, self.intent.pk
            ))

        not_fulfilled_state = self.intent_fulfilment_info.not_fulfilled_state

        match not_fulfilled_state:
            case [NotFulfilledState.FULFILMENTFAILED, NotFulfilledState.TERMINATED, NotFulfilledState.ACKNOWLEDGED]:
                raise serializers.ValidationError(
                    'Cannot perform target update on intent={}. Intent is in state={}'.format(
                        self.intent.pk, not_fulfilled_state
                    ))

            case _:
                pass

        return attrs

    def update(self, instance, validated_data):
        target_id = validated_data.get('target_id')
        target_value_range_data = validated_data.get('target_value_range').get('data')
        target_value_range_type = validated_data.get('target_value_range').get('_type')

        expectation_target = self.expectation_targets_mapping[target_id]
        target_value_range_qs = expectation_target.target_value_ranges.all()

        with transaction.atomic():
            match target_value_range_type:
                case ValueRangeType.SelectedType.TYPE_TIME_WINDOW:
                    for target_value_range_obj, target_value_range_value in zip(list(target_value_range_qs, target_value_range_data)):
                        target_value_range_obj.type_time_window = target_value_range_value
                        target_value_range_obj.save()
                case _:
                    target_value_range_obj = target_value_range_qs[0]
                    target_value_range_obj.selected_type = target_value_range_type
                    setattr(target_value_range_obj, target_value_range_type, target_value_range_data)
                    target_value_range_obj.save()

            self.intent_fulfilment_info.not_fulfilled_state = NotFulfilledState.ACKNOWLEDGED
            self.intent_fulfilment_info.fulfilment_status = FulfilmentStatus.NOT_FULFILLED
            self.intent_fulfilment_info.save()

        # Run Feasibility Check
        intent_feasibility_check.delay(self.intent.pk)

        return instance
