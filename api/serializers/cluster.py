import json

from django.conf import settings
from rest_framework import serializers

from nemo.tasks import publish_to_rabbitmq


class ClusterIpfsSerializer(serializers.Serializer):
    link_id = serializers.CharField(help_text='The IPFS link id')
    ipfs_link = serializers.CharField(help_text='The IPFS link to retrieve the cluster config')


class ClusterSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=42, help_text='The ID of the cluster')
    vm_name = serializers.CharField(max_length=42, help_text='The name of the cluster resource')
    cpus = serializers.IntegerField(help_text='The number of the CPUs')
    memory = serializers.IntegerField(help_text='The RAM of the cluster in GB')
    storage = serializers.IntegerField(help_text='The storage of the cluster in GB')
    endpoint = serializers.CharField(help_text='The endpoint of the Cluster')
    ipfs = ClusterIpfsSerializer(help_text='IPFS details')


class ClusterRegisterSerializer(serializers.Serializer):
    cluster_name = serializers.CharField(max_length=42, help_text='The name of the cluster resource')
    cpus = serializers.IntegerField(help_text='The number of the CPUs')
    memory = serializers.IntegerField(help_text='The RAM of the cluster in GB')
    storage = serializers.IntegerField(help_text='The storage of the cluster in GB')

