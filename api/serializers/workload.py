import json
import logging
import tarfile
from io import BytesIO
from pathlib import Path

import sh
from asgiref.sync import async_to_sync
from celery import chain
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from pyhelm3 import Client
from rest_framework import serializers

from api.serializers.intent import IntentTemplateSerializer, IntentTemplateEmbeddedSerializer, IntentSerializer
from api.utils import delete_directory, collect_workload_resources, intent_full_prefetch
from intent.models import Intent
from nemo.constants import IntentUserLabels
from nemo.models import WorkloadDocument, WorkloadDocumentChart, WorkloadDocumentChartMaintainer, \
    WorkloadDocumentChartDependency, WorkloadDocumentChartMetadata, User
from nemo.models.workload import WorkloadDocumentInstance, WorkloadDocumentLifecycleEvent
from nemo.tasks import post_process_workload_document, publish_to_rabbitmq
from nemo.utils import SemVerParser, convert_keys_to_snake_case, set_nemo_identifier, render_ingress_manifest, \
    set_linkerd_injection
from nemo.validators import helm_chart_structure_validator, helm_chart_template_validator, \
    docker_image_access_validator, container_resources_validator, service_ingress_support_validator

LOGGER = logging.getLogger(__name__)


class WorkloadDocumentCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = WorkloadDocument
        fields = ('id', 'status', 'user', 'name', 'version', 'schema', 'type', 'intents', 'ingress_support')
        read_only_fields = ('id', 'status', 'user')

    def validate_intents(self, intents):
        if len(intents) != len(list(set(intents))):
            raise serializers.ValidationError('Intents cannot be duplicates')

        if any(intent not in IntentUserLabels.values for intent in intents):
            raise serializers.ValidationError('Invalid Intent type')

        return intents

    def validate(self, attrs):
        if WorkloadDocument.objects.filter(name=attrs['name'], version=attrs['version']).exists():
            raise serializers.ValidationError(
                f"Workload {attrs['name']} version {attrs['version']} already exists"
            )

        return attrs


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'first_name', 'last_name',)


class WorkloadDocumentChartMetadataSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkloadDocumentChartMetadata
        exclude = ('chart', 'chart_details')


class WorkloadDocumentChartMaintainerSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkloadDocumentChartMaintainer
        fields = '__all__'


class WorkloadDocumentChartDependencySerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkloadDocumentChartDependency
        fields = '__all__'


class WorkloadDocumentChartSerializer(serializers.ModelSerializer):
    maintainers = WorkloadDocumentChartMaintainerSerializer(many=True)
    dependencies = WorkloadDocumentChartDependencySerializer(many=True)
    metadata = WorkloadDocumentChartMetadataSerializer()

    class Meta:
        model = WorkloadDocumentChart
        fields = '__all__'


class WorkloadDocumentListSerializer(serializers.ModelSerializer):
    user = UserSerializer()
    chart = WorkloadDocumentChartSerializer()

    class Meta:
        model = WorkloadDocument
        fields = '__all__'


class WorkloadDocumentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkloadDocument
        fields = ('name', 'version', 'schema', 'type', 'status', 'user', 'intents', 'ingress_support', 'scope')
        read_only_fields = ('status', 'user')

    def validate_intents(self, intents):
        if len(intents) != len(list(set(intents))):
            raise serializers.ValidationError('Intents cannot be duplicates')

        if any(intent not in IntentUserLabels.values for intent in intents):
            raise serializers.ValidationError('Invalid Intent type')

        return intents

    def validate(self, attrs):
        if self.instance.status != WorkloadDocument.WorkloadDocumentStatus.PENDING:
            raise serializers.ValidationError('Cannot update workload document that is not status=pending')

        if self.instance.user != self.context['request'].user:
            raise serializers.ValidationError('Cannot update workload document that is not of the same user')

        new_name = attrs.get('name', self.instance.name)
        new_version = attrs.get('version', self.instance.version)

        if (WorkloadDocument.objects.filter(name=new_name, version=new_version)
                .exclude(id=self.instance.id).exists()):
            raise serializers.ValidationError('WorkloadDocument with this name and version already exists')

        return attrs


class WorkloadDocumentTemplateInputSerializer(serializers.Serializer):
    release_name = serializers.CharField(max_length=100, required=True, help_text='The release name')
    values_override = serializers.JSONField(default={}, help_text='values.yaml override')
    include_crds = serializers.BooleanField(default=False, help_text='Include CRDS')
    is_upgrade = serializers.BooleanField(default=False, help_text='If its upgrade')
    namespace = serializers.CharField(help_text='Namespace to associate with')
    no_hooks = serializers.BooleanField(default=False, help_text='No hooks flag')
    ingress_enabled = serializers.BooleanField(default=False, help_text='Expose workload instance via NEMO')
    cluster_name = serializers.CharField(required=False, help_text='Target cluster override')
    intents = IntentTemplateEmbeddedSerializer(required=False, many=True, help_text='List of intent (templates) to create')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'intents' in self.fields:
            self.fields['intents'].context.update(self.context)

    @staticmethod
    def validate_release_name(value):
        if WorkloadDocumentInstance.objects.filter(release_name=value).exists():
            raise serializers.ValidationError(
                f'Workload document instance with release_name={value} already exists')
        return value

    def validate_ingress_enabled(self, ingress_enabled):
        workload_document = self.context['workload_document']

        if ingress_enabled and not workload_document.ingress_support:
            raise serializers.ValidationError('Ingress support is disabled for workload document: {}'.format(
                workload_document
            ))

        return ingress_enabled

    def validate(self, attrs):
        workload_document = self.context['workload_document']
        client = Client()
        attrs['client'] = client

        try:
            attrs['chart'] = async_to_sync(client.get_chart)(
                workload_document.name,
                repo=settings.HELM_REPOSITORY_BUCKET,
                version=workload_document.version
            )

        except Exception as e:
            LOGGER.error(f'Unable to retrieve helm chart. Reason: {str(e)}', exc_info=True)
            raise serializers.ValidationError('Unable to fetch chart')

        attrs['manifests'] = list(async_to_sync(client.template_resources)(
            attrs['chart'],
            attrs['release_name'],
            attrs['values_override'],
            include_crds=attrs['include_crds'],
            is_upgrade=attrs['is_upgrade'],
            namespace=attrs['namespace'],
            no_hooks=attrs['no_hooks']
        ))

        if attrs['ingress_enabled']:
            service_ingress_support_validation = service_ingress_support_validator(attrs['manifests'])
            if not service_ingress_support_validation.is_valid:
                raise serializers.ValidationError('Invalid service ingress support: {}'.format(
                    service_ingress_support_validation.reason))
            attrs['ingress_metadata'] = service_ingress_support_validation.ingress_metadata


        return attrs

    def create(self, validated_data):
        workload_document = self.context['workload_document']
        client = validated_data.pop('client')
        chart = validated_data.pop('chart')
        manifests = validated_data.pop('manifests')
        ingress_enabled = validated_data.pop('ingress_enabled')
        ingress_metadata = validated_data.pop('ingress_metadata', None)
        cluster_name = validated_data.get('cluster_name', None)

        intents = validated_data.get('intents', [])

        if ingress_enabled:
            ingress_manifest = render_ingress_manifest(ingress_metadata, validated_data['release_name'])
            manifests.append(ingress_manifest)

        try:
            with transaction.atomic():
                # Generate instance
                workload_document_instance = WorkloadDocumentInstance.objects.create(
                    workload_document=workload_document,
                    release_name=validated_data['release_name'],
                    input_metadata=validated_data,
                    ingress_enabled=ingress_enabled,
                    ingress_metadata=ingress_metadata,
                    cluster_name=cluster_name,
                    user=self.context['request'].user,
                )

                workload_document_instance.manifests = set_linkerd_injection(
                    set_nemo_identifier(manifests, workload_document_instance.instance_id))

                workload_document_instance.save()

                WorkloadDocumentLifecycleEvent.objects.create(
                    workload_document_instance=workload_document_instance,
                    type=WorkloadDocumentLifecycleEvent.WorkloadDocumentLifecycleType.RENDERED,
                    timestamp=str(workload_document_instance.created)
                )

                for intent_data in intents:
                    intent_serializer = IntentTemplateEmbeddedSerializer(data=intent_data, context=self.context)
                    intent_serializer.create(intent_data, instance_id=workload_document_instance.instance_id)

        except Exception as e:
            LOGGER.error(f'Exception while creating workload document instance. '
                         f'Reason: {str(e)}', exc_info=True)
            raise serializers.ValidationError('Error while creating workload document instance')

        # Attach intent data
        serialized_data = json.loads(workload_document_instance.serialize())
        intents = Intent.objects.prefetch_related(*intent_full_prefetch).filter(
            intent_expectations__expectation_object__object_instance=workload_document_instance.instance_id)
        serialized_data['intents'] = [IntentSerializer(intent).data for intent in intents]

        # Notify Rabbitmq
        publish_to_rabbitmq.delay(
            json.dumps(serialized_data, cls=DjangoJSONEncoder),
            settings.RABBITMQ_EXCHANGES['workload'],
            settings.RABBITMQ_ROUTING_KEYS['workload_run']
        )

        return workload_document_instance


class WorkloadDocumentUploadSerializer(serializers.Serializer):
    file = serializers.FileField(required=True, help_text='Packaged helm chart in ```.tgz``` extension')
    name = serializers.CharField(required=True, help_text='Workload Name to associate with')
    version = serializers.CharField(required=True, help_text='Workload Version to associate with')

    @staticmethod
    def __cleanup(folder_path: Path) -> None:
        delete_directory(folder_path)

    @staticmethod
    def validate_file(value):
        if not value.name.lower().endswith('.tgz'):
            raise serializers.ValidationError('File must have .tgz extension')
        return value

    @staticmethod
    def validated_version(value):
        try:
            parser = SemVerParser(value)
            parser.parse()
        except ValueError as e:
            raise serializers.ValidationError(str(e))

    def validate(self, attrs):
        workload_document_qs = WorkloadDocument.objects.scope(self.context['request'].user).filter(
            name=attrs['name'],
            version=attrs['version'],
            type=WorkloadDocument.WorkloadDocumentType.CHART,
            status='pending'
        )

        if not len(workload_document_qs):
            raise serializers.ValidationError(
                f"Invalid associated Workload Document f{attrs['name']} v{attrs['version']} type=chart, status=pending"
            )

        workload_document = workload_document_qs[0]

        file = attrs.get('file')

        # Uncompress the file
        file_dir = settings.FILE_UPLOAD_TEMP_DIR / file.name.rstrip('.tgz')
        file_dir.mkdir(parents=True, exist_ok=True)

        with tarfile.open(fileobj=BytesIO(file.read())) as tar:
            tar.extractall(file_dir)

        # Validate helm chart structure
        chart_folder = next(file_dir.iterdir())
        chart_structure_validation = helm_chart_structure_validator(chart_folder)

        try:
            if not chart_structure_validation.is_valid:
                raise serializers.ValidationError(f'Invalid helm chart structure: {chart_structure_validation.reason}')

            LOGGER.debug(f'Workload Document {workload_document} passed helm chart structural validation')

            chart_details = chart_structure_validation.chart_details
            chart_values = chart_structure_validation.chart_values

            if chart_details['name'] != workload_document.name \
                    or chart_details['version'] != workload_document.version:
                raise serializers.ValidationError(
                    f"Invalid Chart name/version. Got ({chart_details['name']},{chart_details['version']}) "
                    f"expected ({workload_document.name},{workload_document.version})"
                )

            # Validate helm template
            chart_template_validation = helm_chart_template_validator(chart_folder, chart_version=chart_details['version'])
            if not chart_template_validation.is_valid:
                raise serializers.ValidationError(f'Invalid helm chart template: {chart_template_validation.reason}')

            LOGGER.debug(f'Workload Document {workload_document} passed helm chart template validation')

            manifests = chart_template_validation.manifests

            # Validate docker image access
            docker_image_access_validation = docker_image_access_validator(manifests)
            if not docker_image_access_validation.is_valid:
                raise serializers.ValidationError(f'Invalid docker image access: {docker_image_access_validation.reason}')

            LOGGER.debug(f'Workload Document {workload_document} passed docker image access validation')

            # Validate container resources
            container_resource_validation = container_resources_validator(manifests)
            if settings.HELM_FORCE_CONTAINER_RESOURCES:
                if not container_resource_validation.is_valid:
                    raise serializers.ValidationError(f'Invalid container resources: {container_resource_validation.reason}')
                LOGGER.debug(f'Workload Document {workload_document} passed container resources validation')

            ingress_metadata = None
            if workload_document.ingress_support:
                service_ingress_support_validation = service_ingress_support_validator(manifests)
                if not service_ingress_support_validation.is_valid:
                    raise serializers.ValidationError(f'Invalid service ingress support: {service_ingress_support_validation.reason}')

                LOGGER.debug(f'Workload Document {workload_document} passed ingress support validation')
                ingress_metadata = service_ingress_support_validation.ingress_metadata

            attrs['workload_document'] = workload_document
            attrs['chart_details'] = chart_details
            attrs['chart_values'] = chart_values
            attrs['manifests'] = manifests
            attrs['image_mapping'] = docker_image_access_validation.image_mapping
            attrs['container_resources_mapping'] = collect_workload_resources(
                container_resource_validation.resource_mapping)
            attrs['ingress_metadata'] = ingress_metadata

            return attrs
        finally:
            self.__cleanup(file_dir)

    def create(self, validated_data):
        file_obj = validated_data.get('file')
        file_path = settings.FILE_UPLOAD_TEMP_DIR / file_obj.name

        # Save the temp file
        storage = FileSystemStorage(location=settings.FILE_UPLOAD_TEMP_DIR)

        # Overwrite existing
        if storage.exists(file_obj.name):
            storage.delete(file_obj.name)

        storage.save(file_obj.name, file_obj)
        chart_pushed = False

        try:
            sh.helm.s3.push('--ignore-if-exists', str(file_path), settings.HELM_REPOSITORY_NAME)
            chart_pushed = True
        except sh.ErrorReturnCode as e:
            LOGGER.error(f'Failed to push chart. Reason: {str(e)}', exc_info=True)
        finally:
            storage.delete(file_obj.name)

        if not chart_pushed:
            raise serializers.ValidationError('Failed to contact NEMO Helm repository')

        try:
            transaction_error = False
            with transaction.atomic():
                chart_details = convert_keys_to_snake_case(validated_data['chart_details'])
                chart_dependencies = chart_details.pop('dependencies', [])
                chart_maintainers = chart_details.pop('maintainers', [])

                workload_document_chart = WorkloadDocumentChart.objects.create(
                    **chart_details
                )

                workload_document_chart_maintainers = []
                for maintainer in chart_maintainers:
                    workload_document_chart_maintainers.append(
                        WorkloadDocumentChartMaintainer(
                            chart=workload_document_chart,
                            **maintainer
                        )
                    )

                WorkloadDocumentChartMaintainer.objects.bulk_create(workload_document_chart_maintainers)

                workload_document_chart_dependencies = []
                for dependency in chart_dependencies:
                    workload_document_chart_dependencies.append(
                        WorkloadDocumentChartDependency(
                            chart=workload_document_chart,
                            **dependency
                        )
                    )

                WorkloadDocumentChartDependency.objects.bulk_create(workload_document_chart_dependencies)

                resource_mappings = validated_data['container_resources_mapping']

                WorkloadDocumentChartMetadata.objects.create(
                    chart=workload_document_chart,
                    container_registries=validated_data['image_mapping'],
                    chart_details=validated_data['chart_details'],
                    manifests=validated_data['manifests'],
                    values=validated_data['chart_values'],
                    resource_mappings=resource_mappings,
                    memory_requests=resource_mappings['_total']['requests']['memory'],
                    memory_limits=resource_mappings['_total']['limits']['memory'],
                    cpu_requests=resource_mappings['_total']['requests']['cpu'],
                    cpu_limits=resource_mappings['_total']['limits']['cpu']
                )

                workload_document = validated_data['workload_document']
                workload_document.chart = workload_document_chart
                workload_document.status = WorkloadDocument.WorkloadDocumentStatus.ONBOARDING
                workload_document.save()
        except Exception as e:
            LOGGER.error(f'Exception while uploading Workload Document Chart. Reason:{str(e)}', exc_info=True)
            transaction_error = True

        # Rollback Helm Chart repository entry
        if transaction_error and chart_pushed:
            try:
                sh.helm.s3.delete(
                    chart_details['name'],
                    '--version',
                    chart_details['version'],
                    settings.HELM_REPOSITORY_NAME
                )
            except sh.ErrorReturnCode as e:
                LOGGER.error(f'Failed to delete chart. Reason: {str(e)}', exc_info=True)

        if transaction_error:
            raise serializers.ValidationError('Error while uploading Workload Document')


        post_process_workload_document.delay(workload_document.pk)


        return workload_document


class WorkloadDocumentLifecycleEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = WorkloadDocumentLifecycleEvent
        exclude = ('workload_document_instance', )


class WorkloadDocumentInstanceSerializer(serializers.ModelSerializer):
    lifecycle_events = WorkloadDocumentLifecycleEventSerializer(many=True)
    user = UserSerializer()

    class Meta:
        model = WorkloadDocumentInstance
        exclude = ('manifests', 'input_metadata')
