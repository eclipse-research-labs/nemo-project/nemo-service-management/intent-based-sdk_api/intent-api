from django.db.models import Q
from django_filters import rest_framework as filters

from intent.models import Intent, NotFulfilledState, FulfilmentStatus
from nemo.models import WorkloadDocument
from nemo.models.workload import WorkloadDocumentInstance


class WorkloadDocumentFilter(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = WorkloadDocument
        fields = ('name', 'version')


class WorkloadDocumentInstanceFilter(filters.FilterSet):
    release_name = filters.CharFilter(field_name='release_name', lookup_expr='icontains')
    cluster_name = filters.CharFilter(field_name='cluster_name', lookup_expr='icontains')

    class Meta:
        model = WorkloadDocumentInstance
        fields = ('instance_id', 'release_name', 'cluster_name', 'workload_document', 'status')


class IntentFilter(filters.FilterSet):
    object_instance = filters.CharFilter(method='workload_instance_id_filter')
    fulfilment_status = filters.MultipleChoiceFilter(
        field_name='intent_reports__intent_fulfilment_report__intent_fulfilment_info__fulfilment_status',
        choices=FulfilmentStatus.choices,
        conjoined=False
    )
    not_fulfilled_state = filters.MultipleChoiceFilter(
        field_name='intent_reports__intent_fulfilment_report__intent_fulfilment_info__not_fulfilled_state',
        choices=NotFulfilledState.choices,
        conjoined=False
    )
    intent_id = filters.NumberFilter(field_name='id')

    def workload_instance_id_filter(self, queryset, name, value):
        return queryset.filter(
            Q(intent_expectations__expectation_object__object_instance=value) |
            Q(intent_contexts__context_value_ranges__type_string=value)
        )

    class Meta:
        model = Intent
        fields = ('user_label', 'object_instance', 'fulfilment_status')