from rest_framework import serializers


class ValueRangeFieldMixin:
    def to_internal_value(self, data):
        from api.utils import value_range_serializer
        return {
            '_type': value_range_serializer(data, case=self.case),
            'data': data
        }

    def to_representation(self, value):
        match self.case:
            case 'context':
                qs = value.instance.context_value_ranges.all()
            case _:
                qs = value.instance.target_value_ranges.all()

        match len(qs):
            case 1:
                return getattr(qs[0], qs[0].selected_type)
            case 2:
                arr = []
                for q in qs:
                    time_window = q.type_time_window
                    if time_window.start_time:
                        arr.append({'start_time': time_window.start_time})
                    else:
                        arr.append({'end_time': time_window.end_time})
                return arr
            case _:
                raise ValueError


class TargetValueRangeField(ValueRangeFieldMixin, serializers.Field):
    case = 'target'


class ContextValueRangeField(ValueRangeFieldMixin, serializers.Field):
    case = 'context'
