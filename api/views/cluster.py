import json
import logging

import requests
from django.conf import settings
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.permissions import NemoRolePermission
from api.serializers import ClusterSerializer, ClusterRegisterSerializer
from api.swagger.responses import CLUSTER_LIST_RESPONSES, CLUSTER_RETRIEVE_RESPONSES, POST_BASE_RESPONSES
from keycloak_integration.authentication import KeycloakAuthentication

from nemo.tasks import publish_to_rabbitmq

LOGGER = logging.getLogger(__name__)


class ClusterListView(ListAPIView):
    """DEPRECATED

    Retrieve cluster details from MOCA component
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    serializer_class = ClusterSerializer
    nemo_roles = ['nemo_provider', 'nemo_partner']

    @swagger_auto_schema(deprecated=True, responses=CLUSTER_LIST_RESPONSES)
    def get(self, request, *args, **kwargs):
        r = requests.get(
            settings.MOCA_ENDPOINT + '/moca/retrieve'
        )

        try:
            if r.status_code == status.HTTP_200_OK:
                return Response(self.serializer_class(r.json(), many=True).data, status=r.status_code)
            return Response(r.json(), status=r.status_code)
        except Exception as e:
            LOGGER.error('Exception while contacting MOCA component. Reason: {}'.format(
                str(e)
            ), exc_info=True)
            return Response({'reason': 'Unexpected error'}, status=status.HTTP_400_BAD_REQUEST)


class ClusterRetrieveView(RetrieveAPIView):
    """DEPRECATED

    Retrieve a single cluster details from MOCA component
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    serializer_class = ClusterSerializer
    nemo_roles = ['nemo_provider', 'nemo_partner']

    @swagger_auto_schema(deprecated=True, responses=CLUSTER_RETRIEVE_RESPONSES)
    def get(self, request, *args, **kwargs):
        r = requests.get(
            settings.MOCA_ENDPOINT + f'/moca/retrieve/{kwargs["id"]}'
        )

        try:
            if r.status_code == status.HTTP_200_OK:
                return Response(self.serializer_class(r.json(), many=True).data, status=r.status_code)
            return Response(r.json(), status=r.status_code)
        except Exception as e:
            LOGGER.error('Exception while contacting MOCA component. Reason: {}'.format(
                str(e)
            ), exc_info=True)
            return Response({'reason': 'Unexpected error'}, status=status.HTTP_400_BAD_REQUEST)


class ClusterRegisterView(CreateAPIView):
    """DEPRECATED

    Registers a kubernetes cluster through the MOCA component.

    This endpoint writes a message to the rabbitmq topic that MOCA component listens to.
    This is performed in asynchronous manner.
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    serializer_class = ClusterRegisterSerializer
    nemo_roles = ['nemo_provider', 'nemo_partner']

    @swagger_auto_schema(deprecated=True, responses=POST_BASE_RESPONSES)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            validated_data = serializer.validated_data
            publish_to_rabbitmq.delay(
                json.dumps(validated_data),
                settings.RABBITMQ_EXCHANGES['cluster'],
                settings.RABBITMQ_ROUTING_KEYS['cluster_register']
            )
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

