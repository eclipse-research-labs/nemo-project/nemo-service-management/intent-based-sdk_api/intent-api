from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.swagger.responses import DELETE_BASE_RESPONSES, LOGIN_RESPONSES


class LoginView(ObtainAuthToken):
    """Create a new auth token for the user
    """
    @swagger_auto_schema(responses=LOGIN_RESPONSES)
    def post(self, request, *args, **kwargs):
        return super(LoginView, self).post(request, *args, **kwargs)


class LogoutView(APIView):
    """Clears the token associated with the user.
    """
    permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]

    @swagger_auto_schema(responses=DELETE_BASE_RESPONSES)
    def post(self, request, *args, **kwargs):
        Token.objects.filter(user=request.user).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)