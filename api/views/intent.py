from drf_yaml.parsers import YAMLParser
from drf_yaml.renderers import YAMLRenderer
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.authentication import TokenAuthentication, BasicAuthentication
from rest_framework.generics import get_object_or_404
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from django_filters import rest_framework as filters
from rest_framework.response import Response
from rest_framework.views import APIView

from api.filters import IntentFilter
from api.parsers import CamelCaseYAMLParser
from api.permissions import NemoRolePermission
from api.renderers import YamlListRenderer
from api.serializers.intent import IntentSerializer, IntentInputAttributeSerializer, IntentTypeSerializer, \
    IntentTemplateSerializer, IntentActionInputSerializer, IntentTargetUpdateSerializer
from api.swagger.responses import INTENT_CREATE_RESPONSES, INTENT_ACTION_RESPONSES, INTENT_TARGET_UPDATE_RESPONSES
from api.utils import intent_full_prefetch
from intent.models import Intent
from keycloak_integration.authentication import KeycloakAuthentication
from nemo.constants import IntentUserLabels


class IntentListCreateView(generics.ListCreateAPIView):
    """List / Creates Intents.

    Filters `fulfilment_status`, `not_fulfilment_state` can accept multiple values.

    The following parsers are supported:
        - `application/yaml`

    The following renderers are supported:
        - `application/yaml`
        - `application/json`

    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, BasicAuthentication, KeycloakAuthentication]
    parser_classes = [CamelCaseYAMLParser]
    renderer_classes = [YAMLRenderer, JSONRenderer]
    serializer_class = IntentSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = IntentFilter
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return IntentSerializer
        return IntentInputAttributeSerializer

    def get_queryset(self):
        return Intent.objects.scope(self.request.user).prefetch_related(*intent_full_prefetch)

    @swagger_auto_schema(request_body=IntentInputAttributeSerializer, responses=INTENT_CREATE_RESPONSES)
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        intent = serializer.create(serializer.validated_data)
        return Response({'intent': intent.pk}, status=status.HTTP_201_CREATED)


class IntentTypeListView(APIView):
    """Lists the valid intent types
    """
    renderer_classes = [JSONRenderer]
    serializer_class = IntentTypeSerializer

    def get(self, request, *args, **kwargs):
        return Response({'types': IntentUserLabels.values})


class IntentTemplateView(APIView):
    """Creates an Intent with the given template
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = IntentTemplateSerializer
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(request_body=IntentTemplateSerializer, responses=INTENT_CREATE_RESPONSES)
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data, context={'request': request})
        if serializer.is_valid():
            validated_data = serializer.validated_data
            intent = serializer.create(validated_data)
            return Response({'intent': intent}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class IntentActionView(APIView):
    """Perform an action to a given Intent
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    renderer_classes = [JSONRenderer]
    serializer_class = IntentActionInputSerializer
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(request_body=IntentActionInputSerializer, responses=INTENT_ACTION_RESPONSES)
    def put(self, request, *args, **kwargs):
        intent = get_object_or_404(
            Intent.objects.scope(request.user).prefetch_related(*intent_full_prefetch),
            pk=kwargs['pk'],
        )

        serializer = self.serializer_class(data=request.data, instance=intent)
        if serializer.is_valid():
            validated_data = serializer.validated_data
            serializer.update(intent, validated_data)
            return Response({}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class IntentTargetUpdateView(APIView):
    """Updates an intent target with a new value.

    Intent has to be in a valid state.
    It is best to use this in a rest api tool, e.g. postman and send data via ``application/yaml`` in order to derive data types better.
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    parser_classes = [CamelCaseYAMLParser, JSONParser]
    renderer_classes = [JSONRenderer]
    serializer_class = IntentTargetUpdateSerializer
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(request_body=IntentTargetUpdateSerializer, responses=INTENT_TARGET_UPDATE_RESPONSES)
    def put(self, request, *args, **kwargs):
        intent = get_object_or_404(
            Intent.objects.scope(request.user).prefetch_related(*intent_full_prefetch),
            pk=kwargs['pk'],
        )

        serializer = self.serializer_class(data=request.data, instance=intent)
        if serializer.is_valid():
            validated_data = serializer.validated_data
            serializer.update(intent, validated_data)
            return Response({}, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)