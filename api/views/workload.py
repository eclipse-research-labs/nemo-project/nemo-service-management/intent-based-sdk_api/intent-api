import json

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, get_object_or_404, ListAPIView, \
    DestroyAPIView, UpdateAPIView
from rest_framework.parsers import MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from django_filters import rest_framework as filters

from api.filters import WorkloadDocumentFilter, WorkloadDocumentInstanceFilter
from api.permissions import NemoRolePermission
from api.renderers import YamlListRenderer
from api.serializers import WorkloadDocumentUploadSerializer, WorkloadDocumentCreateSerializer, \
    WorkloadDocumentListSerializer, WorkloadDocumentUpdateSerializer, WorkloadDocumentTemplateInputSerializer, \
    WorkloadDocumentInstanceSerializer
from api.swagger.responses import POST_BASE_RESPONSES, INSTANCE_ID, WORKLOAD_INSTANCE_MANIFESTS_RESPONSES
from keycloak_integration.authentication import KeycloakAuthentication
from nemo.models import WorkloadDocument
from nemo.models.workload import WorkloadDocumentInstance


class WorkloadDocumentTemplateView(APIView):
    """Renders kubernetes manifests for given workload document.

    Set header ```Accept``` to ```application/json``` or ```application/yaml``` (default).

    This action creates a workload document instance with a unique NEMO workload identifier (```instance_id```).

    RabbitMQ is notified as per README.md
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    renderer_classes = [YamlListRenderer, JSONRenderer]
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(request_body=WorkloadDocumentTemplateInputSerializer, responses=POST_BASE_RESPONSES)
    def post(self, request, *args, **kwargs):
        workload_document = get_object_or_404(
            WorkloadDocument.objects.scope(request.user),
            id=self.kwargs.get('pk'),
            status=WorkloadDocument.WorkloadDocumentStatus.ACCEPTED,
            type=WorkloadDocument.WorkloadDocumentType.CHART
        )

        ctx = {'workload_document': workload_document, 'request': request}

        serializer = WorkloadDocumentTemplateInputSerializer(data=request.data, context=ctx)

        if serializer.is_valid():
            workload_document_instance = serializer.create(serializer.validated_data)
            return Response(workload_document_instance.manifests)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WorkloadListCreateView(ListCreateAPIView):
    """List or Create a new workload document(s)
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = WorkloadDocumentFilter
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return WorkloadDocumentCreateSerializer
        return WorkloadDocumentListSerializer

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        qs = WorkloadDocument.objects.visible(self.request.user).select_related(
            'user', 'chart__metadata'
        ).prefetch_related(
            'chart__maintainers',
            'chart__dependencies'
        )

        print(len(qs))
        return qs


class WorkloadDocumentRetrieveUpdateDestroyView(RetrieveUpdateDestroyAPIView):
    """Retrieve, update and delete a workload document

    Update & Delete operations are only allowed when a workload document is in ```status=pending``` and the same user
    if performing the operation.

    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    queryset = WorkloadDocument.objects.none()
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return WorkloadDocumentListSerializer
        return WorkloadDocumentUpdateSerializer

    def get_object(self):
        return get_object_or_404(WorkloadDocument.objects.scope(user=self.request.user).select_related(
            'user', 'chart__metadata'
        ).prefetch_related(
            'chart__maintainers',
            'chart__dependencies'
        ), pk=self.kwargs.get('pk'))

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.status != WorkloadDocument.WorkloadDocumentStatus.PENDING:
            return Response(
                'Cannot delete workload document that is not status=pending',
                status=status.HTTP_400_BAD_REQUEST
            )

        if instance.user != request.user:
            return Response(
                'Cannot delete workload document that is not of the same user',
                status=status.HTTP_400_BAD_REQUEST
            )

        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class WorkloadUploadView(APIView):
    """Upload a workload document underlying helm chart.

    The following must apply:
    - The helm chart must be packed as ```.tgz``` (by running helm package).
    - The helm chart must have a matching (name, version) pair with the associated workload document.
    - The helm chart must have a valid structure, files ```Chart.yaml```, ```values.yaml``` and folder ```templates``` are mandatory.
    - The helm chart must be able to render (via helm template) without any errors.
    - The helm chart underlying containers images must exist and be reachable by Nemo Intent API (either public or private registries with appropriate imagePullSecrets).

    If everything is OK, the helm chart is uploaded to the NEMO S3 Helm Repository.

    After successful upload, the Workload Document is set to ```status=onboarding``` for further validation.
    After successful validation, the Workload Document is set to ```status=accepted``` or ```status=rejected``` if validation has failed.

    RabbitMQ is notified as per README.md
    """
    # - The helm chart must render all pods with ```requests/limis``` set for both ```cpu``` and ```memory```.

    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    parser_classes = (MultiPartParser,)
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(request_body=WorkloadDocumentUploadSerializer, responses=POST_BASE_RESPONSES)
    def post(self, request, *args, **kwargs):
        serializer = WorkloadDocumentUploadSerializer(data=request.data, context={'request': request})

        if serializer.is_valid():
            serializer.create(serializer.validated_data)
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class WorkloadDocumentInstanceView(ListAPIView):
    """Lists all the workload documents instances
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    serializer_class = WorkloadDocumentInstanceSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = WorkloadDocumentInstanceFilter
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    def get_queryset(self):
        return WorkloadDocumentInstance.objects.scope(self.request.user)


class WorkloadDocumentInstanceManifestsView(APIView):
    """Fetch workload instance manifests in a single ``.yaml`` format.
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    renderer_classes = [YamlListRenderer, JSONRenderer]
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    @swagger_auto_schema(manual_parameters=[INSTANCE_ID], responses=WORKLOAD_INSTANCE_MANIFESTS_RESPONSES)
    def get(self, request, *args, **kwargs):
        workload_document_instance = get_object_or_404(
            WorkloadDocumentInstance.objects.scope(request.user),
            instance_id=self.kwargs.get('instance_id'),
        )

        return Response(workload_document_instance.manifests)


class WorkloadDocumentInstanceDeleteView(APIView):
    """Propagate a workload document instance deletion for a deployed workload document instance to the M.O..
    """
    permission_classes = [IsAuthenticated, NemoRolePermission]
    authentication_classes = [TokenAuthentication, KeycloakAuthentication]
    nemo_roles = ['nemo_provider', 'nemo_consumer']

    def put(self, request, *args, **kwargs):
        workload_document_instance = get_object_or_404(
            WorkloadDocumentInstance.objects.scope(request.user),
            instance_id=self.kwargs.get('instance_id'),
            status=WorkloadDocumentInstance.WorkloadDocumentInstanceStatus.DEPLOYED,
        )

        payload = {
            'id': workload_document_instance.id,
            'instance_id': workload_document_instance.instance_id,
            'cluster_name': workload_document_instance.cluster_name,

        }

        from nemo.tasks import publish_to_rabbitmq

        publish_to_rabbitmq.delay(
            json.dumps(payload, cls=DjangoJSONEncoder),
            settings.RABBITMQ_EXCHANGES['workload'],
            settings.RABBITMQ_ROUTING_KEYS['workload_delete']
        )


        return Response(status=status.HTTP_200_OK)