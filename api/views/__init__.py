from .auth import (
    LoginView,
    LogoutView
)

from .workload import (
    WorkloadListCreateView,
    WorkloadUploadView,
    WorkloadDocumentRetrieveUpdateDestroyView,
    WorkloadDocumentTemplateView,
    WorkloadDocumentInstanceView,
    WorkloadDocumentInstanceManifestsView,
    WorkloadDocumentInstanceDeleteView
)

from .cluster import (
    ClusterListView,
    ClusterRetrieveView,
    ClusterRegisterView
)

from .intent import (
    IntentListCreateView,
    IntentTypeListView,
    IntentTemplateView,
    IntentActionView,
    IntentTargetUpdateView
)