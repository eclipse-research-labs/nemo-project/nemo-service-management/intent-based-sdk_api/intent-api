"""
Define default swagger responses
"""
from copy import deepcopy

from drf_yasg import openapi
from rest_framework import status, serializers

from api.serializers import ClusterSerializer
from api.serializers.intent import IntentOutputSerializer

GENERIC_RESPONSES = {
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
}

POST_BASE_RESPONSES = {
    status.HTTP_201_CREATED: 'Created',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
    status.HTTP_404_NOT_FOUND: 'Resource not found'
}

RETRIEVE_BASE_RESPONSES = {
    status.HTTP_200_OK: 'Ok',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
    status.HTTP_404_NOT_FOUND: 'Resource not found'
}

RETRIEVE_LIST_BASE_RESPONSES = {
    status.HTTP_200_OK: 'Ok',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
}

RETRIEVE_BASE_RESPONSES = {
    status.HTTP_200_OK: 'Ok',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
    status.HTTP_404_NOT_FOUND: 'Resource not found'
}

UPDATE_BASE_RESPONSES = {
    status.HTTP_200_OK: 'Ok',
    status.HTTP_400_BAD_REQUEST: 'Provided data is invalid or malformed',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
    status.HTTP_404_NOT_FOUND: 'Resource not found'
}

DELETE_BASE_RESPONSES = {
    status.HTTP_204_NO_CONTENT: 'No Content',
    status.HTTP_401_UNAUTHORIZED: 'Invalid credentials',
    status.HTTP_403_FORBIDDEN: 'Forbidden from performing this action',
    status.HTTP_404_NOT_FOUND: 'Resource not found'
}


CLUSTER_LIST_RESPONSES = deepcopy(RETRIEVE_BASE_RESPONSES)
CLUSTER_LIST_RESPONSES.update({status.HTTP_200_OK: ClusterSerializer(many=True)})

CLUSTER_RETRIEVE_RESPONSES = deepcopy(CLUSTER_LIST_RESPONSES)

INTENT_CREATE_RESPONSES = deepcopy(POST_BASE_RESPONSES)
INTENT_CREATE_RESPONSES.update({status.HTTP_201_CREATED: IntentOutputSerializer})

INTENT_ACTION_RESPONSES = deepcopy(UPDATE_BASE_RESPONSES)
INTENT_TARGET_UPDATE_RESPONSES = deepcopy(UPDATE_BASE_RESPONSES)

WORKLOAD_INSTANCE_MANIFESTS_RESPONSES = deepcopy(RETRIEVE_BASE_RESPONSES)
WORKLOAD_INSTANCE_MANIFESTS_RESPONSES.update({
    status.HTTP_200_OK: openapi.Response(
        description="Kubernetes Manifests",
        schema=openapi.Schema(
            type=openapi.TYPE_ARRAY,
            items=openapi.Schema(
                type=openapi.TYPE_OBJECT,
            ),
        ),
    )
})
# ==================================
INSTANCE_ID = openapi.Parameter(
    name='instance_id',
    in_=openapi.IN_PATH,
    type=openapi.TYPE_STRING,
    description='The Workload Document instance uuid',
    required=True
)

class TokenSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=40, help_text='Token string')

LOGIN_RESPONSES = deepcopy(RETRIEVE_BASE_RESPONSES)
LOGIN_RESPONSES.update({status.HTTP_200_OK: TokenSerializer})