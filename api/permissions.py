from rest_framework import permissions


class NemoRolePermission(permissions.BasePermission):
    """NEMO Role based permissions
    """
    def has_permission(self, request, view):
        # Ensure the user is authenticated
        if not request.user or not request.user.is_authenticated:
            return False

        if not hasattr(view, 'nemo_roles'):
            # No nemo role restriction for this view
            return True

        # Check if the user has any of the required roles
        user_roles = request.user.user_roles.all().values_list('title', flat=True)

        return any(role in user_roles for role in view.nemo_roles)