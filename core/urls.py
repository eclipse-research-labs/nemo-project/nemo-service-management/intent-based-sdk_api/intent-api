from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('backend/management/admin/', admin.site.urls),
    path('healthz/', include('health_check.urls')),
    path('api/v1/', include('api.urls'))
]
