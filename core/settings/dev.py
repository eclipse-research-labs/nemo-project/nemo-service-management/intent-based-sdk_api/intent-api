import os
from .base import *

DEBUG = True
ALLOWED_HOSTS = ['*']

# =================================
#        DATABASE SETTINGS
# =================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_DATABASE', default='nemo'),
        'USER': os.getenv('POSTGRES_USER', default='postgres'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD', default='12345678'),
        'HOST': os.getenv('POSTGRES_HOST', default='127.0.0.1'),
        'PORT': os.getenv('POSTGRES_PORT', default='5428'),
    }
}

# ==================================
#   REDIS SETTINGS
# ==================================
REDIS_HOST = os.getenv('REDIS_HOST', default='127.0.0.1')
REDIS_PORT = os.getenv('REDIS_PORT', default=6376)
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', None)
REDIS_CELERY_DATABASE = os.getenv('REDIS_CELERY_DATABASE', 0)
REDIS_URL = 'redis://:{}@{}:{}/{}'.format(
    REDIS_PASSWORD, REDIS_HOST, REDIS_PORT, REDIS_CELERY_DATABASE
) if REDIS_PASSWORD else \
    'redis://{}:{}/{}'.format(
        REDIS_HOST, REDIS_PORT, REDIS_CELERY_DATABASE
    )
REDIS_CELERY_URI = REDIS_URL


# =================================
#   RABBITMQ SETTINGS
# =================================
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST', default='127.0.0.1')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT', default=5672)
RABBITMQ_USER = os.getenv('RABBITMQ_USER', default='user')
RABBITMQ_PASSWORD = os.getenv('RABBITMQ_PASSWORD', default='password')
RABBITMQ_EXCHANGES = {
    'workload': 'nemo.api.workload',
    'cluster': 'nemo.api.cluster'
}
RABBITMQ_ROUTING_KEYS = {
    'workload_upload': 'upload',
    'workload_run': 'run',
    'workload_update': 'update',
    'workload_delete': 'delete',
    'cluster_register': 'upload',
    'intent_notify': 'intent-notify',
    'intent_collector': 'intent',
}

# =================================
#   CELERY SETTINGS
# =================================
CELERY_BROKER_URL = REDIS_CELERY_URI
CELERY_BROKER_CONNECTION_RETRY_ON_STARTUP = True
CELERY_RESULT_BACKEND = REDIS_CELERY_URI
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
RETRY_OPTIONS = {
    'retry_backoff': 5,
    'retry_kwargs': {
        'max_retries': 10,
        'retry_backoff_max': 60 * 60
    }
}

CELERY_ACKS_LATE = True


# =================================
#   HELM SETTINGS
# =================================
HELM_REPOSITORY_BUCKET = os.getenv(
    'HELM_REPOSITORY_BUCKET',
    default='s3://nemo-helm-repository-37db8f4f-da53-4e2f-bde1-3f153b582433/charts'
)


HELM_REPOSITORY_NAME = os.getenv(
    'HELM_REPOSITORY_NAME',
    default='nemo-charts'
)

HELM_FORCE_CONTAINER_RESOURCES = bool(os.getenv('HELM_FORCE_CONTAINER_RESOURCES', default=False))

# =================================
#   MOCA SETTINGS
# =================================
MOCA_ENDPOINT = 'http://127.0.0.1:8001'


# =================================
#   KEYCLOAK CONFIGURATION
# =================================
KEYCLOAK_BASE_URL = os.getenv('KEYCLOAK_BASE_URL')
KEYCLOAK_REALM_NAME = os.getenv('KEYCLOAK_REALM_NAME')
KEYCLOAK_CLIENT_ID = os.getenv('KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_SECRET = os.getenv('KEYCLOAK_CLIENT_SECRET')
KEYCLOAK_SERVER_URL = f'{KEYCLOAK_BASE_URL}/admin'
KEYCLOAK_AUTH_URI = f'{KEYCLOAK_BASE_URL}/realms/{KEYCLOAK_REALM_NAME}'

# =================================
#   CORS SETTINGS
# =================================
CORS_ALLOWED_ORIGINS = [
    'http://localhost:8080'
]