import os
from .base import *

DEBUG = False
SECRET_KEY = os.getenv('SECRET_KEY')
ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS', default='*').split(',')
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# =================================
#        DATABASE SETTINGS
# =================================
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.getenv('POSTGRES_DATABASE'),
        'USER': os.getenv('POSTGRES_USER'),
        'PASSWORD': os.getenv('POSTGRES_PASSWORD'),
        'HOST': os.getenv('POSTGRES_HOST'),
        'PORT': os.getenv('POSTGRES_PORT'),
    }
}

# ==================================
#   REDIS SETTINGS
# ==================================
REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD', None)
REDIS_CELERY_DATABASE = os.getenv('REDIS_CELERY_DATABASE')
REDIS_CACHE_DATABASE = os.getenv('REDIS_CACHE_DATABASE')
REDIS_URL = 'redis://:{}@{}:{}'.format(
    REDIS_PASSWORD, REDIS_HOST, REDIS_PORT
) if REDIS_PASSWORD else \
    'redis://{}:{}'.format(
        REDIS_HOST, REDIS_PORT
    )

REDIS_CELERY_URI = REDIS_URL + '/{}'.format(REDIS_CELERY_DATABASE)
REDIS_CACHE_URL = REDIS_URL + '/{}'.format(REDIS_CACHE_DATABASE)


# =================================
#   RABBITMQ SETTINGS
# =================================
RABBITMQ_HOST = os.getenv('RABBITMQ_HOST')
RABBITMQ_PORT = os.getenv('RABBITMQ_PORT')
RABBITMQ_USER = os.getenv('RABBITMQ_USER')
RABBITMQ_PASSWORD = os.getenv('RABBITMQ_PASSWORD')
RABBITMQ_EXCHANGES = {
    'workload': 'nemo.api.workload',
    'cluster': 'nemo.api.cluster'
}
RABBITMQ_ROUTING_KEYS = {
    'workload_upload': 'upload',
    'workload_run': 'run',
    'workload_update': 'update',
    'workload_delete': 'delete',
    'cluster_register': 'upload',
    'intent_notify': 'intent-notify',
    'intent_collector': 'intent'
}

# =================================
#   CELERY SETTINGS
# =================================
CELERY_BROKER_URL = REDIS_CELERY_URI
CELERY_BROKER_CONNECTION_RETRY_ON_STARTUP = True
CELERY_RESULT_BACKEND = REDIS_CELERY_URI
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'UTC'
RETRY_OPTIONS = {
    'retry_backoff': 5,
    'retry_kwargs': {
        'max_retries': 10,
        'retry_backoff_max': 60 * 60
    }
}

CELERY_ACKS_LATE = True

# ==================================
#   CACHE SETTINGS
# ==================================
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.redis.RedisCache",
        "LOCATION": REDIS_CACHE_URL,
    }
}

# ==================================
#   SESSION SETTINGS
# ==================================
SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"

# =================================
#   HELM SETTINGS
# =================================
HELM_REPOSITORY_BUCKET = os.getenv('HELM_REPOSITORY_BUCKET')
HELM_REPOSITORY_NAME = os.getenv('HELM_REPOSITORY_NAME')
HELM_FORCE_CONTAINER_RESOURCES = bool(os.getenv('HELM_FORCE_CONTAINER_RESOURCES', default=False))

# =================================
#   MOCA SETTINGS
# =================================
MOCA_ENDPOINT = os.getenv('MOCA_ENDPOINT')

# =================================
#   KEYCLOAK CONFIGURATION
# =================================
KEYCLOAK_BASE_URL = os.getenv('KEYCLOAK_BASE_URL')
KEYCLOAK_REALM_NAME = os.getenv('KEYCLOAK_REALM_NAME')
KEYCLOAK_CLIENT_ID = os.getenv('KEYCLOAK_CLIENT_ID')
KEYCLOAK_CLIENT_SECRET = os.getenv('KEYCLOAK_CLIENT_SECRET')
KEYCLOAK_SERVER_URL = f'{KEYCLOAK_BASE_URL}/admin'
KEYCLOAK_AUTH_URI = f'{KEYCLOAK_BASE_URL}/realms/{KEYCLOAK_REALM_NAME}'

# =================================
#   CORS SETTINGS
# =================================
CORS_ALLOWED_ORIGINS = split_env_on_comma("CORS_ALLOWED_ORIGINS")