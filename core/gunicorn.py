import multiprocessing

cpu_count = multiprocessing.cpu_count()
workers = 2 * cpu_count + 1
worker_class = "gthread"
threads = 2 * cpu_count
user = 0
group = 0
accesslog = "-"
errorlog = "-"
bind = ["0.0.0.0:8000"]
