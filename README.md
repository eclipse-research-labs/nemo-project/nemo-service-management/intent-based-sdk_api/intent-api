Nemo Intent API
=

Prerequisites
===

* Docker
* docker-compose
* helm
* skopeo
* s3cmd (optional)

Helm repository guide
===

Initialization
=====
Install s3 plugin:

```bash
helm plugin install https://github.com/hypnoglow/helm-s3.git
```

Export the necessary environmental variables:

```bash
export AWS_ENDPOINT=<s3 url>
export AWS_ACCESS_KEY_ID=<s3 access key>
export AWS_SECRET_ACCESS_KEY=<s3 secret key>
export AWS_DEFAULT_REGION=<s3 region>
export HELM_REPOSITORY_BUCKET=<bucket path with /charts ending>
export HELM_REPOSITORY_NAME=<bucker repository name>
```

Initialize the helm repository:

```bash
helm s3 init --ignore-if-exists ${HELM_REPOSITORY_BUCKET}
```

Add the helm repository reference:

```bash
helm repo add ${HELM_REPOSITORY_NAME} ${HELM_REPOSITORY_BUCKET}
```

Listing stored charts
=====

Listing all uploaded charts:

```bash
s3cmd ls --host=${AWS_ENDPOINT} --access_key=${AWS_ACCESS_KEY_ID} --secret_key=${AWS_SECRET_ACCESS_KEY} --region=${AWS_DEFAULT_REGION} --host-bucket=  ${HELM_REPOSITORY_BUCKET}/
```

Retrieving repository index file:

```bash
s3cmd get ${HELM_REPOSITORY_BUCKET}/index.yaml --host=${AWS_ENDPOINT} --access_key=${AWS_ACCESS_KEY_ID} --secret_key=${AWS_SECRET_ACCESS_KEY} --region=${AWS_DEFAULT_REGION} --host-bucket= - | cat
 ```

Development
===

Launching local postgresql & redis instance:

```bash
docker-compose -up -d -f config/local/docker-compose.yml
```

Rabbitmq integration
===

The following table contains the integration effort of RabbitMQ component thus far:

|   Exchange Name   | Exchange Type |             Queues             |  Routing Key  |  Message Type    | Message Example                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | Publishing reason                                                 |
|:-----------------:|:-------------:|:------------------------------:|:-------------:|------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------|
| nemo.api.workload |     direct    |     [upload (durable), ...]    |     upload    | stringified JSON | '{"id": 4, "name": "echo-server", "version": "0.5.5", "type": "chart", "status": "accepted"}'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | workload lifecycle status={onboarding, accepted, rejected}        |
| nemo.api.workload |     direct    |      [run (durable), ...]      |      run      | stringified JSON | '{"id": 6, "instance_id": "936623ae-8969-4d73-8c04-527d1cb815ac", "workload_document_id": 9, "release_name": "dasddadasdaasdda", "status": "rendered", "manifests": [], "cluster_name": null, 'intents': [{'id': 34, 'user_label': 'DeliverComputingWorkload', 'intent_preemption_capability': 'FALSE', 'observation_period': 60, 'intent_expectations': [{'id': 30, 'expectation_id': '1', 'expectation_verb': 'ENSURE', 'expectation_object': {'id': 31, 'object_type': 'NEMO_WORKLOAD', 'object_instance': '936623ae-8969-4d73-8c04-527d1cb815ac', 'context_selectivity': None, 'object_contexts': []}, 'expectation_targets': [{'id': 64, 'target_name': 'cpuUsage', 'target_condition': 'IS_LESS_THAN', 'target_value_range': 20, 'target_contexts': []}], 'expectation_contexts': []}], 'intent_report_reference': {'id': 22, 'intent_fulfilment_report': {'id': 22, 'intent_fulfilment_info': {'fulfilment_status': 'NOT_FULFILLED', 'not_fulfilled_state': 'ACKNOWLEDGED', 'not_fulfilled_reasons': []}, 'expectation_fulfilment_results': []}, 'intent_feasibility_check_report': None, 'last_updated_time': '2024-10-21T10:43:35.982522Z'}, 'intent_contexts': []}]}'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | workload instance lifecycle status={rendered,}                    |
| nemo.api.workload |     direct    | [intent-notify (durable), ...] | intent-notify | stringified JSON | '{"id": 28, "user_label": "FederatedLearning", "intent_preemption_capability": "FALSE", "observation_period": 60, "intent_expectations": [{"id": 24, "expectation_id": "1", "expectation_verb": "DELIVER", "expectation_object": {"id": 25, "object_type": "NEMO_WORKLOAD", "object_instance": "e4708935-c65b-4159-9c81-8e33524ca1e7", "context_selectivity": null, "object_contexts": []}, "expectation_targets": [{"id": 58, "target_name": "clusterType", "target_condition": "IS_EQUAL_TO", "target_value_range": "FederatedLearning", "target_contexts": []}], "expectation_contexts": []}], "intent_report_reference": {"id": 16, "intent_fulfilment_report": {"id": 16, "intent_fulfilment_info": {"fulfilment_status": "NOT_FULFILLED", "not_fulfilled_state": "COMPLIANT", "not_fulfilled_reasons": []}, "expectation_fulfilment_results": [{"expectation_fulfilment_info": {"fulfilment_status": "NOT_FULFILLED", "not_fulfilled_state": "ACKNOWLEDGED", "not_fulfilled_reasons": []}, "expectation_id": 24, "target_fulfilment_results": [{"target": 58, "target_achieved_value": null, "target_fulfilment_info": {"fulfilment_status": "NOT_FULFILLED", "not_fulfilled_state": "ACKNOWLEDGED", "not_fulfilled_reasons": []}}]}]}, "intent_feasibility_check_report": {"id": 14, "feasibility_check_type": "FEASIBLE", "infeasibility_reason": null}, "last_updated_time": "2024-07-09T14:17:36.590890Z"}, "intent_contexts": []}' | intent creation or update and Feasibility Check passed (is valid) |
| nemo.api.workload |     direct    | [delete (durable), ...]        | delete        | stringified JSON | '{"id": 23, "instance_id": "5ce430d6-28d7-4775-b60b-17991e72c842", "cluster_name": "k3s-cluster"}'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            | workload instance deletion                                        |

Exposing a Workload Document Instance via NEMO:
===

1. The workload document must have set the ```ingress_support=True``` during workload creation
2. During workload instantiation, the value ```ingress_enabled=True``` must be set alongside the following annotations inside a Service object:


|          Annotation          |   Type   |                          Default value                         |                   Description                   |
|:----------------------------:|:--------:|:--------------------------------------------------------------:|:-----------------------------------------------:|
| nemo.eu/ingress-expose       | Required | "true"                                                         | Marks the service as exposable via NEMO Ingress |
| nemo.eu/ingress-service-port | Optional | If not set it defaults to the port of the Service marked above | The assosiated Service port                     |
| nemo.eu/ingress-path         | Optional | "/"                                                            | Ingress path to expose                          |
| nemo.eu/ingress-path-type    | Optional | "ImplementationSpecific"                                       | Ingress path type                               |