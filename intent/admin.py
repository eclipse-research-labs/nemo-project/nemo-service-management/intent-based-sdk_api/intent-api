from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from intent.models import Intent, IntentExpectation, ExpectationObject, ExpectationTarget, Context, ContextValueRange, \
    TimeWindow, TargetValueRange, TargetFulfilmentResult, ExpectationFulfilmentResult, IntentReport


@admin.register(Intent)
class IntentAdmin(admin.ModelAdmin):
    list_display = ('id', 'user_label', 'context_selectivity', 'intent_admin_state', 'observation_period')
    list_filter = ('intent_admin_state', 'context_selectivity')
    search_fields = ('user_label',)


@admin.register(ExpectationTarget)
class ExpectationTargetAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_intent_expectation_link', 'target_name', 'target_condition')
    list_filter = ('target_condition',)

    def get_intent_expectation_link(self, obj):
        link = reverse('admin:intent_intentexpectation_change', args=[obj.intent_expectation.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_intent_expectation_link.short_description = 'Intent Expectation'


class ExpectationTargetInline(admin.TabularInline):
    model = ExpectationTarget
    fields = ('target_name', 'target_condition', 'get_target_value_range')
    readonly_fields = ('get_target_value_range',)
    extra = 1

    def get_target_value_range(self, obj):
        from api.serializers.intent import ExpectationTargetSerializer
        return ExpectationTargetSerializer(instance=obj).data['target_value_range']

    get_target_value_range.short_description = 'Target Value Range'


class ObjectContextInline(admin.TabularInline):
    model = Context
    fields = ('context_attribute', 'context_condition', 'get_context_value_range')
    readonly_fields = ('get_context_value_range',)

    def get_context_value_range(self, obj):
        from api.serializers.intent import ContextSerializer
        return ContextSerializer(instance=obj).data['context_value_range']

    get_context_value_range.short_description = 'Context Value Range'


@admin.register(ExpectationObject)
class ExpectationObjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'object_type', 'object_instance', 'context_selectivity')
    list_filter = ('object_type',)
    search_fields = ('object_instance',)
    inlines = [ObjectContextInline]


@admin.register(IntentExpectation)
class IntentExpectationAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_intent_link', 'expectation_id', 'expectation_verb', 'get_expectation_object_link')
    list_filter = ('expectation_verb',)
    search_fields = ('expectation_id',)
    inlines = [ExpectationTargetInline]

    def get_intent_link(self, obj):
        link = reverse('admin:intent_intent_change', args=[obj.intent.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_intent_link.short_description = 'Intent'

    def get_expectation_object_link(self, obj):
        link = reverse('admin:intent_expectationobject_change', args=[obj.expectation_object.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_expectation_object_link.short_description = 'Expectation Object'


@admin.register(Context)
class ContextAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'get_intent_link', 'get_intent_expectation_link', 'get_expectation_object_link', 'get_expectation_target_link',
    'context_attribute', 'context_condition')
    list_filter = ('context_condition',)

    def get_intent_link(self, obj):
        if obj.intent_id:
            link = reverse('admin:intent_intent_change', args=[obj.intent.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_intent_link.short_description = 'Intent'

    def get_intent_expectation_link(self, obj):
        if obj.intent_expectation_id:
            link = reverse('admin:intent_intentexpectation_change', args=[obj.intent_expectation.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_intent_expectation_link.short_description = 'Intent Expectation'

    def get_expectation_object_link(self, obj):
        if obj.expectation_object_id:
            link = reverse('admin:intent_expectationobject_change', args=[obj.expectation_object.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_expectation_object_link.short_description = 'Expectation Object'

    def get_expectation_target_link(self, obj):
        if obj.expectation_target_id:
            link = reverse('admin:intent_expectationtarget_change', args=[obj.expectation_target.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_expectation_target_link.short_description = 'Expectation Target'


@admin.register(TimeWindow)
class TimeWindowAdmin(admin.ModelAdmin):
    list_display = ('id', 'start_time', 'end_time')


@admin.register(ContextValueRange)
class ContextValueRangeAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'get_context_link', 'selected_type', 'type_integer', 'type_boolean', 'type_string', 'type_number',
    'type_datetime', 'get_type_time_window_link')
    list_filter = ('selected_type',)

    def get_context_link(self, obj):
        link = reverse('admin:intent_context_change', args=[obj.context.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_context_link.short_description = 'Context'

    def get_type_time_window_link(self, obj):
        if obj.type_time_window_id:
            link = reverse('admin:intent_timewindow_change', args=[obj.type_time_window.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_type_time_window_link.short_description = 'Time Window'


@admin.register(TargetValueRange)
class TargetValueRangeAdmin(admin.ModelAdmin):
    list_display = (
    'id', 'get_expectation_target_link', 'selected_type', 'type_integer', 'type_boolean', 'type_string', 'type_number',
    'type_datetime', 'get_type_time_window_link')
    list_filter = ('selected_type',)

    def get_expectation_target_link(self, obj):
        link = reverse('admin:intent_expectationtarget_change', args=[obj.expectation_target.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_expectation_target_link.short_description = 'Expectation Target'

    def get_type_time_window_link(self, obj):
        if obj.type_time_window_id:
            link = reverse('admin:intent_timewindow_change', args=[obj.type_time_window.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        return None

    get_type_time_window_link.short_description = 'Time Window'


@admin.register(TargetFulfilmentResult)
class TargetFulfilmentResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_expectation_target_link', 'target_achieved_value', 'get_target_fulfilment_info')

    def get_expectation_target_link(self, obj):
        link = reverse('admin:intent_expectationtarget_change', args=[obj.target.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_expectation_target_link.short_description = 'Expectation Target'

    def get_target_fulfilment_info(self, obj):
        return str(obj.target_fulfilment_info)

    get_target_fulfilment_info.short_description = 'Fulfilment Info'


@admin.register(ExpectationFulfilmentResult)
class ExpectationFulfilmentResultAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_intent_expectation_link', 'get_expectation_fulfilment_info')

    def get_intent_expectation_link(self, obj):
        link = reverse('admin:intent_intentexpectation_change', args=[obj.expectation.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_intent_expectation_link.short_description = 'Intent Expectation'

    def get_expectation_fulfilment_info(self, obj):
        return str(obj.expectation_fulfilment_info)

    get_expectation_fulfilment_info.short_description = 'Fulfilment Info'


@admin.register(IntentReport)
class IntentReporttAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_intent_link', 'get_intent_fulfilment_info', 'last_updated_time')

    def get_intent_link(self, obj):
        link = reverse('admin:intent_intent_change', args=[obj.intent_reference.id])
        return format_html('<a href="{}">{}</a>', link, obj)

    get_intent_link.short_description = 'Intent'

    def get_intent_fulfilment_info(self, obj):
        return str(obj.intent_fulfilment_report.intent_fulfilment_info)

    get_intent_fulfilment_info.short_description = 'Fulfilment Info'