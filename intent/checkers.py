from collections import defaultdict
from random import randint
from time import sleep
from dateutil import parser
from django.utils import timezone

from intent.expections import NemoFeasibilityError
from intent.models import IntentFeasibilityReport


class ValidatorMeta(type):
    def __new__(cls, name, bases, dct):
        new_cls = super().__new__(cls, name, bases, dct)
        new_cls.validators = {}
        for attr_name, attr_value in dct.items():
            if attr_name.startswith("validate_") and callable(attr_value):
                new_cls.validators[attr_name] = attr_value
        return new_cls


class IntentFeasibilityChecker(metaclass=ValidatorMeta):
    def __init__(self, intent):
        self.intent = intent
        self.now = timezone.now()

        # Objects
        # id => object
        self.intent_expectations = {}
        self.expectation_targets = {}
        self.expectation_objects = {}
        self.expectation_object_contexts = {}

        # Mappings
        # intent_expectation_id => expectation target
        self.expectation_targets_mapping = defaultdict(list)
        # intent_expectation_id => expectation object
        self.expectation_object_mapping = {}
        # expectation_object_id => object context
        self.expectation_object_contexts_mapping = defaultdict(list)
        # object_context_id => context_value ranges
        self.context_value_ranges_mapping = defaultdict(list)

        intent_expectations = self.intent.intent_expectations.all()

        for intent_expectation in intent_expectations:
            self.intent_expectations[intent_expectation.id] = intent_expectation
            expectation_targets = intent_expectation.expectation_targets.all()

            for expectation_target in expectation_targets:
                self.expectation_targets[expectation_target.id] = expectation_target
                self.expectation_targets_mapping[intent_expectation.id] = expectation_target

            expectation_object = intent_expectation.expectation_object
            self.expectation_objects[expectation_object.id] = expectation_object
            self.expectation_object_mapping[intent_expectation.id] = expectation_object
            object_contexts = expectation_object.object_contexts.all()
            self.expectation_object_contexts_mapping[expectation_object.id] = object_contexts

            for object_context in object_contexts:
                self.expectation_object_contexts[object_context.id] = object_context
                context_value_ranges = object_context.context_value_ranges.all()
                self.context_value_ranges_mapping[object_context.id] = context_value_ranges

    def validate_dates(self):
        for expectation_object_id, object_contexts in self.expectation_object_contexts_mapping.items():
            for object_context in object_contexts:
                context_value_ranges = self.context_value_ranges_mapping[object_context.id]
                if object_context.context_attribute == 'serviceEndTime' \
                        and getattr(context_value_ranges[0], context_value_ranges[0].selected_type) < self.now:
                    raise NemoFeasibilityError('ObjectContext={} of Intent={} for Workload={} has serviceEndTime exceeded'.format(
                        object_context.pk, self.intent.user_label, self.expectation_objects[expectation_object_id].object_instance)
                    )

    def check(self):
        try:
            for validator in self.validators.values():
                validator(self)
            return IntentFeasibilityReport.FeasibilityCheckResult.FEASIBLE, None
        except NemoFeasibilityError as e:
            return IntentFeasibilityReport.FeasibilityCheckResult.INFEASIBLE, str(e)