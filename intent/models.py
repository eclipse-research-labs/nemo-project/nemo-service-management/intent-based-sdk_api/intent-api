#   title: Intent NRM
#   version: 18.3.0
#   description: >-
#     OAS 3.0.1 definition of the Intent NRM
#     © 2024, 3GPP Organizational Partners (ARIB, ATIS, CCSA, ETSI, TSDSI, TTA, TTC).
#     All rights reserved.
#
#   3GPP TS 28.312; Intent driven management services for mobile networks
#   url: http://www.3gpp.org/ftp/Specs/archive/28_series/28.312/
from django.contrib.postgres.fields import ArrayField
from django.db import models

from intent.apps import IntentConfig
from nemo.managers.intent import IntentManager


class FulfilmentStatus(models.TextChoices):
    """
     It describes the current status of the intent fulfilment result.
    """
    FULFILLED = 'FULFILLED', 'FULFILLED'
    NOT_FULFILLED = 'NOT_FULFILLED', 'NOT_FULFILLED'


class NotFulfilledState(models.TextChoices):
    """
    It describes the current progress of or the reason for not achieving fulfilment
    for the intent, intentExpectation or expectationTarget.
    An attribute which is used when FulfilmentInfo is implemented for IntentFulfilmentInfo
    """
    ACKNOWLEDGED = 'ACKNOWLEDGED', 'ACKNOWLEDGED'
    COMPLIANT = 'COMPLIANT', 'COMPLIANT'
    DEGRADED = 'DEGRADED', 'DEGRADED'
    SUSPENDED = 'SUSPENDED', 'SUSPENDED'
    TERMINATED = 'TERMINATED', 'TERMINATED'
    FULFILMENTFAILED = 'FULFILMENTFAILED', 'FULFILMENTFAILED'


class Selectivity(models.TextChoices):
    ALL_OF = 'ALL_OF', 'ALL_OF'
    ONE_OF = 'ONE_OF', 'ONE_OF'
    ANY_OF = 'ANY_OF', 'ANY_OF'


class Condition(models.TextChoices):
    IS_EQUAL_TO = 'IS_EQUAL_TO', 'IS_EQUAL_TO'
    IS_LESS_THAN = 'IS_LESS_THAN', 'IS_LESS_THAN'
    IS_GREATER_THAN = 'IS_GREATER_THAN', 'IS_GREATER_THAN'
    IS_WITHIN_RANGE = 'IS_WITHIN_RANGE', 'IS_WITHIN_RANGE'
    IS_OUTSIDE_RANGE = 'IS_OUTSIDE_RANGE', 'IS_OUTSIDE_RANGE'
    IS_ONE_OF = 'IS_ONE_OF', 'IS_ONE_OF'
    IS_NOT_ONE_OF = 'IS_NOT_ONE_OF', 'IS_NOT_ONE_OF'
    IS_EQUAL_TO_OR_LESS_THAN = 'IS_EQUAL_TO_OR_LESS_THAN', 'IS_EQUAL_TO_OR_LESS_THAN'
    IS_EQUAL_TO_OR_GREATER_THAN = 'IS_EQUAL_TO_OR_GREATER_THAN', 'IS_EQUAL_TO_OR_GREATER_THAN'
    IS_ALL_OF = 'IS_ALL_OF', 'IS_ALL_OF'


class IntentActionChoices(models.TextChoices):
    SUSPEND = 'suspend', 'suspend'
    RESUME = 'resume', 'resume'
    TERMINATE = 'terminate', 'terminate'


class TimeWindow(models.Model):
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'time_window'
        verbose_name = 'Time Window'
        verbose_name_plural = 'Time Windows'

    def __str__(self):
        return f'{self.start_time} - {self.end_time}'


class Context(models.Model):
    intent = models.ForeignKey('Intent', on_delete=models.CASCADE, related_name='intent_contexts', blank=True, null=True)
    intent_expectation = models.ForeignKey('IntentExpectation', on_delete=models.CASCADE, related_name='expectation_contexts', blank=True, null=True)
    expectation_object = models.ForeignKey('ExpectationObject', on_delete=models.CASCADE, related_name='object_contexts', blank=True, null=True)
    expectation_target = models.ForeignKey('ExpectationTarget', on_delete=models.CASCADE, related_name='target_contexts', blank=True, null=True)
    context_attribute = models.CharField(max_length=50)
    context_condition = models.CharField(max_length=30, choices=Condition, default=Condition.IS_EQUAL_TO, db_index=True)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'context'
        verbose_name = 'Context'
        verbose_name_plural = 'Contexts'

    def __str__(self):
        return f'{self.context_attribute} - {self.context_condition}'


class ValueRangeType(models.Model):
    class SelectedType(models.TextChoices):
        TYPE_INTEGER = 'type_integer', 'type_integer'
        TYPE_BOOLEAN = 'type_boolean', 'type_boolean'
        TYPE_STRING = 'type_string', 'type_string'
        TYPE_NUMBER = 'type_number', 'type_number'
        TYPE_DATETIME = 'type_datetime', 'type_datetime'
        TYPE_TIME_WINDOW = 'type_time_window', 'type_time_window'

    selected_type = models.CharField(max_length=50, choices=SelectedType, default=SelectedType.TYPE_STRING, db_index=True)
    """
    ONE OF
    """
    type_integer = models.IntegerField(blank=True, null=True)
    type_boolean = models.BooleanField(blank=True, null=True)
    type_string = models.CharField(max_length=256, blank=True, null=True)
    type_number = models.FloatField(blank=True, null=True)
    type_datetime = models.DateTimeField(blank=True, null=True)
    type_time_window = models.ForeignKey(TimeWindow, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        abstract = True


class ContextValueRange(ValueRangeType):
    """
    The value will be a single value when the contextCondition is either "IS_EQUAL_TO", "IS_LESS_THAN", "IS_GREATER_THAN", "IS_EQUAL_TO_OR_LESS_THAN", "IS_EQUAL_TO_OR_GREATER_THAN".
    The value will be a pair of values when the contextCondition is either "IS_WITHIN_RANGE", "IS_OUTSIDE_RANGE"
    The value will be a list when the contextCondition is "IS_ONE_OF", "IS_NOT_ONE_OF","IS_ALL_OF".
    """
    context = models.ForeignKey(Context, on_delete=models.CASCADE, related_name='context_value_ranges')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'context_value_range'
        verbose_name = 'Context Value Range'
        verbose_name_plural = 'Context Value Ranges'

    def __str__(self):
        return f'{self.selected_type}'


class TargetValueRange(ValueRangeType):
    expectation_target = models.ForeignKey('ExpectationTarget', on_delete=models.CASCADE, related_name='target_value_ranges')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'target_value_range'
        verbose_name = 'Target Value Range'
        verbose_name_plural = 'Target Values Ranges'

    def __str__(self):
        return f'{self.selected_type}'


class Intent(models.Model):
    """
    This IOC represents the properties of an Intent driven management information between MnS consumer and MnS producer.
    """
    class IntentAdminState(models.TextChoices):
        ACTIVATED = 'ACTIVATED', 'ACTIVATED'
        DEACTIVATED = 'DEACTIVATED', 'DEACTIVATED'

    class IntentPreemptionCapability(models.TextChoices):
        TRUE = 'TRUE', 'TRUE'
        FALSE = 'FALSE', 'FALSE'

    user_label = models.CharField(max_length=50, help_text="A user-friendly (and user assignable) name of the intent.")
    context_selectivity = models.CharField(max_length=6, choices=Selectivity, blank=True, null=True, db_index=True, help_text="How to select among the stated intentContexts")
    intent_admin_state = models.CharField(max_length=11, choices=IntentAdminState, default=IntentAdminState.ACTIVATED, db_index=True, help_text="It describes the intent administrative state.  This attribute is used when MnS consumer-suspension mechanism is supported")
    intent_priority = models.PositiveIntegerField(default=1, help_text="It expresses the priority of the stated intent within a MnS consumer")
    intent_preemption_capability = models.CharField(max_length=5, choices=IntentPreemptionCapability, default=IntentPreemptionCapability.FALSE, db_index=True)
    observation_period = models.PositiveIntegerField(default=60, help_text="In seconds")

    objects = IntentManager()

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent'
        verbose_name = 'Intent'
        verbose_name_plural = 'Intents'

    def __str__(self):
        return f'{self.user_label}'


class IntentExpectation(models.Model):
    """
    This data type is the "IntentExpectation" data type without specialisations
    It represents MnS consumer's requirements, goals and contexts given to a 3GPP system
    """
    class ExpectationVerb(models.TextChoices):
        DELIVER = 'DELIVER', 'DELIVER'
        ENSURE = 'ENSURE', 'ENSURE'
        CREATE = 'CREATE', 'CREATE'

    intent = models.ForeignKey(Intent, on_delete=models.CASCADE, related_name='intent_expectations')
    expectation_id = models.CharField(max_length=256, help_text="A unique identifier of the intentExpectation within the intent")
    expectation_verb = models.CharField(max_length=7, choices=ExpectationVerb, default=ExpectationVerb.DELIVER, db_index=True)
    expectation_object = models.ForeignKey('ExpectationObject', on_delete=models.CASCADE, related_name='intent_expectations')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent_expectation'
        verbose_name = 'Intent Expectation'
        verbose_name_plural = 'Intent Expectations'

    def __str__(self):
        return f'{self.expectation_id}'


class ExpectationObject(models.Model):
    """
    It represents the Object to which the IntentExpectation should apply.
    This data type is the "ExpectationObject" data type without specialisations
    """
    class ObjectType(models.TextChoices):
        RAN_SUBNETWORK = 'RAN_SUBNETWORK', 'RAN_SUBNETWORK'
        EDGE_SERVICE_SUPPORT = 'EDGE_SERVICE_SUPPORT', 'EDGE_SERVICE_SUPPORT'
        _5GC_SUBNETWORK = '5GC_SUBNETWORK', '5GC_SUBNETWORK'
        NEMO_WORKLOAD = 'NEMO_WORKLOAD', 'NEMO_WORKLOAD'
        _5G_SLICE_FLOW = '5G_SLICE_FLOW', '5G_SLICE_FLOW'
        L2SM_NETWORK = 'L2SM_NETWORK', 'L2SM_NETWORK'

    object_type = models.CharField(max_length=20, choices=ObjectType, default=ObjectType.NEMO_WORKLOAD, help_text='It describes the expectation object type which can be supported by a specific intent handling function of MnS producer. ')
    object_instance = models.CharField(max_length=256, db_index=True)
    context_selectivity = models.CharField(max_length=6, choices=Selectivity, blank=True, null=True, db_index=True, help_text="How to select among the stated expectationContexts")

    class Meta:
        app_label = IntentConfig.name
        db_table = 'expectation_object'
        verbose_name = 'Expectation Object'
        verbose_name_plural = 'Expectation Objects'

    def __str__(self):
        return f'{self.object_type} - {self.object_instance}'


class ExpectationTarget(models.Model):
    """
    This data type represents the target of the IntentExpectation that are required to be achieved.
    This data type is the "ExpectationTarget" data type without specialisations
    """
    intent_expectation = models.ForeignKey(IntentExpectation, on_delete=models.CASCADE, related_name='expectation_targets')
    target_name = models.CharField(max_length=50)
    target_condition = models.CharField(max_length=30, choices=Condition, db_index=True)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'expectation_target'
        verbose_name = 'Expectation Target'
        verbose_name_plural = 'Expectation Targets'

    def __str__(self):
        return f'{self.target_name} - {self.target_condition}'


class FulfilmentInfo(models.Model):
    """
     This dataType represents the properties of a specific fulfilment information for an aspect of
    the intent (i.e. either an expectation, a target or the whole intent).
    """
    fulfilment_status = models.CharField(max_length=20, choices=FulfilmentStatus, default=FulfilmentStatus.NOT_FULFILLED, db_index=True)
    not_fulfilled_state = models.CharField(max_length=20, choices=NotFulfilledState, default=NotFulfilledState.ACKNOWLEDGED, db_index=True)
    not_fulfilled_reasons = ArrayField(models.TextField(blank=True, null=True), default=list)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'fulfilment_info'
        verbose_name = 'Fulfilment Info'
        verbose_name_plural = 'Fulfilments Info'

    def __str__(self):
        return f'{self.fulfilment_status} - {self.not_fulfilled_state}'


class TargetFulfilmentResult(models.Model):
    """
    This data type includes targetFulfilmentInfo and targetAchievedValue for each ExpectationTarget.
    """
    target = models.ForeignKey(ExpectationTarget, on_delete=models.CASCADE, related_name='target_fulfilment_results')
    target_achieved_value = models.CharField(max_length=256, blank=True, null=True)
    target_fulfilment_info = models.ForeignKey(FulfilmentInfo, on_delete=models.CASCADE, related_name='target_fulfilment_results')
    expectation_fulfilment_result = models.ForeignKey('ExpectationFulfilmentResult', on_delete=models.CASCADE, related_name='target_fulfilment_results')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'target_fulfilment_result'
        verbose_name = 'Target Fulfilment Result'
        verbose_name_plural = 'Target Fulfilment Results'

    def __str__(self):
        return f'{self.target} - {self.target_achieved_value}'


class ExpectationFulfilmentResult(models.Model):
    """
    It includes the expectationFulfilmentInfo and targetFulfilmentResults for each IntentExpectation.
    """
    expectation = models.ForeignKey(IntentExpectation, on_delete=models.CASCADE, related_name='expectation_fulfilment_results')
    expectation_fulfilment_info = models.ForeignKey(FulfilmentInfo, on_delete=models.CASCADE, related_name='expectation_fulfilment_results')
    intent_fulfilment_report = models.ForeignKey('IntentFulfilmentReport', on_delete=models.CASCADE, related_name='expectation_fulfilment_results')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'expectation_fulfilment_result'
        verbose_name = 'Expectation Fulfilment Result'
        verbose_name_plural = 'Expectation Fulfilment Results'

    def __str__(self):
        return f'{self.expectation} - {self.expectation_fulfilment_info}'


class IntentFulfilmentReport(models.Model):
    """
    It includes the intentFulfilmentInfo and expectationFulfilmetResult.
    This attribute shall be supported when intent fulfilment information is supported by IntentReport
    """
    intent_fulfilment_info = models.ForeignKey(FulfilmentInfo, on_delete=models.CASCADE, related_name='intent_fulfilment_reports')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent_fulfilment_report'
        verbose_name = 'Intent Fulfilment Report'
        verbose_name_plural = 'Intent Fulfilment Reports'

    def __str__(self):
        return f'{self.intent_fulfilment_info}'


class IntentConflictReport(models.Model):
    """
    It represents the conflict information for the detected conflict
    This attribute shall be supported when intent conflict information is supported by IntentReport
    """
    class ConflictType(models.TextChoices):
        INTENT_CONFLICT = 'INTENT_CONFLICT', 'INTENT_CONFLICT'
        EXPECTATION_CONFLICT = 'EXPECTATION_CONFLICT', 'EXPECTATION_CONFLICT'
        TARGET_CONFLICT = 'TARGET_CONFLICT', 'TARGET_CONFLICT'

    class RecommendedSolutions(models.TextChoices):
        MODIFY = 'MODIFY', 'MODIFY'
        DELETE = 'DELETE', 'DELETE'

    intent_report = models.ForeignKey('IntentReport', on_delete=models.CASCADE, related_name='intent_conflict_reports')
    conflict_type = models.CharField(max_length=30, choices=ConflictType, db_index=True)
    conflicting_intent = models.ForeignKey(Intent, on_delete=models.CASCADE, related_name='conflicting_intent_reports', blank=True, null=True)
    conflicting_target = models.ForeignKey(ExpectationTarget, on_delete=models.CASCADE, related_name='conflicting_target_reports', blank=True, null=True)
    conflicting_expectation = models.ForeignKey(IntentExpectation, on_delete=models.CASCADE, related_name='conflicting_expectation_reports', blank=True, null=True)
    recommended_solutions = models.CharField(max_length=30, choices=RecommendedSolutions, blank=True, null=True)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent_conflict_report'
        verbose_name = 'Intent Conflict Report'
        verbose_name_plural = 'Intent Conflict Reports'

    def __str__(self):
        return f'{self.conflict_type}'


class IntentFeasibilityReport(models.Model):
    """
    It represents the intent feasibility check information
    This attribute shall be supported when intent feasibility check information information is supported by IntentReport
    """
    class FeasibilityCheckResult(models.TextChoices):
        FEASIBLE = 'FEASIBLE', 'FEASIBLE'
        INFEASIBLE = 'INFEASIBLE', 'INFEASIBLE'

    feasibility_check_type = models.CharField(max_length=10, choices=FeasibilityCheckResult, db_index=True)
    infeasibility_reason = models.TextField(blank=True, null=True, help_text='An attribute which is used when feasibilityCheckResult is INFEASIBLE')

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent_feasibility_report'
        verbose_name = 'Intent Feasibility Report'
        verbose_name_plural = 'Intent Feasibility Reports'

    def __str__(self):
        return f'{self.feasibility_check_type}'


class IntentReport(models.Model):
    """
    It represents intent report information from MnS producer to MnS consumer
    """
    intent_reference = models.ForeignKey(Intent, on_delete=models.CASCADE, related_name='intent_reports')
    intent_fulfilment_report = models.ForeignKey(IntentFulfilmentReport, on_delete=models.CASCADE, related_name='intent_reports', blank=True, null=True)
    intent_feasibility_check_report = models.ForeignKey(IntentFeasibilityReport, on_delete=models.CASCADE, related_name='intent_reports', blank=True, null=True)
    last_updated_time = models.DateTimeField(auto_now=True)

    class Meta:
        app_label = IntentConfig.name
        db_table = 'intent_report'
        verbose_name = 'Intent Report'
        verbose_name_plural = 'Intent Reports'

    def __str__(self):
        return f'{self.intent_reference} - {self.intent_fulfilment_report}'