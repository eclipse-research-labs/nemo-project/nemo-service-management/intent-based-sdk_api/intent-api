from intent.models import Context, ValueRangeType, ContextValueRange, TimeWindow, ExpectationObject, Intent, \
    IntentExpectation, ExpectationTarget, TargetValueRange, FulfilmentStatus, NotFulfilledState
from nemo.constants import IntentUserLabels


def create_context(model_object, context_data):
    context_value_range_dict = context_data.pop('context_value_range')
    selected_type = context_value_range_dict.pop('_type', None)
    context_value_range_data = context_value_range_dict.pop('data')

    context_object = Context(
        **context_data
    )

    match model_object:
        case ExpectationObject():
            context_object.expectation_object = model_object
        case Intent():
            context_object.intent = model_object
        case IntentExpectation():
            context_object.intent_expectation = model_object
        case ExpectationTarget():
            context_object.expectation_target = model_object
        case _:
            raise ValueError('Invalid object class')

    context_object.save()

    match selected_type:
        case ValueRangeType.SelectedType.TYPE_TIME_WINDOW:
            for data in context_value_range_data:
                time_window = TimeWindow.objects.create(**data)
                ContextValueRange.objects.create(
                    context=context_object,
                    selected_type=ValueRangeType.SelectedType.TYPE_TIME_WINDOW,
                    type_time_window=time_window
                )
        case _:
            context_value_range = ContextValueRange(
                context=context_object,
                selected_type=selected_type,
            )

            setattr(context_value_range, selected_type, context_value_range_data)
            context_value_range.save()

    return context_object


def create_target(intent_expectation, target_data):
    target_value_range_dict = target_data.pop('target_value_range')
    selected_type = target_value_range_dict.pop('_type', None)
    target_value_range_data = target_value_range_dict.pop('data')
    target_contexts_data = target_data.pop('target_contexts', [])

    expectation_target = ExpectationTarget.objects.create(
        intent_expectation=intent_expectation,
        **target_data
    )

    for target_context_data in target_contexts_data:
        target_context = create_context(expectation_target, target_context_data)

    match selected_type:
        case ValueRangeType.SelectedType.TYPE_TIME_WINDOW:
            for data in target_value_range_data:
                time_window = TimeWindow.objects.create(**data)
                TargetValueRange.objects.create(
                    expectation_target=target_data,
                    selected_type=ValueRangeType.SelectedType.TYPE_TIME_WINDOW,
                    type_time_window=time_window
                )
        case _:
            target_value_range = TargetValueRange(
                expectation_target=expectation_target,
                selected_type=selected_type,
            )

            setattr(target_value_range, selected_type, target_value_range_data)
            target_value_range.save()


def is_valid_intent_label(label):
    return label in IntentUserLabels.values


def fulfilment_status_transition(old_fulfilment_status, new_fulfilment_status, old_not_fulfilled_state):
    # (prev, current)
    match (old_fulfilment_status, new_fulfilment_status):
        case (FulfilmentStatus.FULFILLED, FulfilmentStatus.FULFILLED):
            new_not_fulfilled_state = NotFulfilledState.COMPLIANT
        case (FulfilmentStatus.FULFILLED, FulfilmentStatus.NOT_FULFILLED):
            new_not_fulfilled_state = NotFulfilledState.DEGRADED
        case (FulfilmentStatus.NOT_FULFILLED, FulfilmentStatus.FULFILLED):
            new_not_fulfilled_state = NotFulfilledState.COMPLIANT
        case (FulfilmentStatus.NOT_FULFILLED, FulfilmentStatus.NOT_FULFILLED):
            new_not_fulfilled_state = NotFulfilledState.DEGRADED
        case _:
            raise ValueError('Invalid state')

    return new_not_fulfilled_state
