import logging
from collections import defaultdict

from django.db import transaction
from django.utils import timezone

from intent.models import Intent, NotFulfilledState, FulfilmentStatus
from intent.utils import fulfilment_status_transition

LOGGER = logging.getLogger(__name__)


class IntentCollector:
    def __init__(self, body):
        self.body = body
        self.timestamp = timezone.now()
        self.invalid_intents = []
        self.valid_intents = []

    def post_process(self):
        LOGGER.info('Parsed #{} valid intents, #{} invalid intents'.format(
            len(self.valid_intents), len(self.invalid_intents)
        ))

    def parse_intent_response(self):
        for intent_response in self.body['intents']:
            intent_id = intent_response.get('id')
            intent_fulfilled = intent_response['fulfilled']

            try:
                from api.utils import intent_full_prefetch
                intent = Intent.objects.prefetch_related(*intent_full_prefetch).get(pk=intent_id)
            except Intent.DoesNotExist:
                LOGGER.error(f'Non existing intent_id: {intent_id}')
                continue

            intent_reports_qs = intent.intent_reports.all()
            intent_report = intent_reports_qs[0]
            intent_fulfilment_report = intent_report.intent_fulfilment_report
            intent_fulfilment_info = intent_fulfilment_report.intent_fulfilment_info
            intent_fulfilment_status = intent_fulfilment_info.fulfilment_status
            intent_not_fulfilled_state = intent_fulfilment_info.not_fulfilled_state

            match intent_not_fulfilled_state:
                case NotFulfilledState.SUSPENDED | NotFulfilledState.FULFILMENTFAILED | NotFulfilledState.TERMINATED:
                    self.invalid_intents.append(intent)
                    LOGGER.warning('Intent={} in invalid state={}'.format(intent.pk, intent_not_fulfilled_state))
                    continue
                case NotFulfilledState.ACKNOWLEDGED:
                    self.invalid_intents.append(intent)
                    LOGGER.warning('Intent={} has reset'.format(intent.pk))
                    continue
                case NotFulfilledState.COMPLIANT | NotFulfilledState.DEGRADED:
                    with transaction.atomic():
                        expectation_response = intent_response['intent_expectations'][0]
                        expectation_fulfilled = expectation_response['fulfilled']
                        expectation_target_response_mapping = {}

                        for expectation_target_response in expectation_response['expectation_targets']:
                            expectation_target_response_mapping[expectation_target_response['target_id']] = \
                                expectation_target_response

                        new_intent_fulfilment_status = FulfilmentStatus.FULFILLED if intent_fulfilled else \
                            FulfilmentStatus.NOT_FULFILLED

                        new_intent_not_fulfilled_state = fulfilment_status_transition(
                            intent_fulfilment_status,
                            new_intent_fulfilment_status,
                            intent_not_fulfilled_state
                        )

                        intent_fulfilment_info.fulfilment_status = new_intent_fulfilment_status
                        intent_fulfilment_info.not_fulfilled_state = new_intent_not_fulfilled_state
                        intent_fulfilment_info.save()
                        intent_report.last_updated_time = self.timestamp
                        intent_report.save()

                        expectation_fulfilment_result_qs = intent_fulfilment_report.expectation_fulfilment_results.all()
                        expectation_fulfilment_result = expectation_fulfilment_result_qs[0]
                        expectation_fulfilment_info = expectation_fulfilment_result.expectation_fulfilment_info
                        expectation_fulfilment_status = expectation_fulfilment_info.fulfilment_status
                        expectation_not_fulfilled_state = expectation_fulfilment_info.not_fulfilled_state
                        new_expectation_fulfilment_status = FulfilmentStatus.FULFILLED if expectation_fulfilled else \
                            FulfilmentStatus.NOT_FULFILLED

                        new_expectation_not_fulfilled_state = fulfilment_status_transition(
                            expectation_fulfilment_status,
                            new_expectation_fulfilment_status,
                            expectation_not_fulfilled_state
                        )

                        expectation_fulfilment_info.fulfilment_status = new_expectation_fulfilment_status
                        expectation_fulfilment_info.not_fulfilled_state = new_expectation_not_fulfilled_state
                        expectation_fulfilment_info.save()

                        target_fulfilment_result_qs = expectation_fulfilment_result.target_fulfilment_results.all()

                        for target_fulfilment_result in target_fulfilment_result_qs:
                            target_id = target_fulfilment_result.target_id
                            expectation_target_response = expectation_target_response_mapping[target_id]
                            target_fulfilled = expectation_target_response['fulfilled']
                            target_fulfilment_info = target_fulfilment_result.target_fulfilment_info
                            target_fulfilment_status = target_fulfilment_info.fulfilment_status
                            target_not_fulfilled_state = target_fulfilment_info.not_fulfilled_state
                            target_fulfilment_result.target_achieved_value = \
                                str(expectation_target_response['target_achieved_value'])
                            new_target_fulfilment_status = FulfilmentStatus.FULFILLED if target_fulfilled else \
                                FulfilmentStatus.NOT_FULFILLED

                            new_target_not_fulfilled_state = fulfilment_status_transition(
                                target_fulfilment_status,
                                new_target_fulfilment_status,
                                target_not_fulfilled_state
                            )

                            target_fulfilment_info.fulfilment_status = new_target_fulfilment_status
                            target_fulfilment_info.not_fulfilled_state = new_target_not_fulfilled_state
                            target_fulfilment_info.save()
                            target_fulfilment_result.save()

                        self.valid_intents.append(intent)
        self.post_process()