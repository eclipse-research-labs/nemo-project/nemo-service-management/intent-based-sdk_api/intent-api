import yaml
from django.conf import settings
from django.core.management import BaseCommand

from intent.collector import IntentCollector
from nemo.rabbitmq import RabbitMQClient
from nemo.utils import convert_keys_to_snake_case


class Command(BaseCommand):
    help = 'Subscribe to rabbitmq Intent Collector component'

    def add_arguments(self, parser):
        parser.add_argument('--queue', type=str, help='Specify queue name')
        parser.add_argument('--durable', action='store_true', help='Specify if queue is durable')

    def handle(self, *args, **options):
        exchange = settings.RABBITMQ_EXCHANGES['workload']
        routing_keys = [settings.RABBITMQ_ROUTING_KEYS['intent_collector']]
        queue = options['queue']
        durable = options['durable']

        def callback(ch, method, properties, body):
            response = convert_keys_to_snake_case(yaml.safe_load(body))
            collector = IntentCollector(response)
            collector.parse_intent_response()

        client = RabbitMQClient()
        client.subscribe(exchange=exchange, routing_keys=routing_keys, callback_fn=callback, queue=queue, durable=durable)