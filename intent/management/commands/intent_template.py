import yaml
from django.core.management import BaseCommand

from api.serializers.intent import IntentInputSerializer, ExpectationTargetSerializer, ContextSerializer, \
    IntentInputAttributeSerializer, IntentTemplateInputSerializer, IntentTemplateSerializer
from api.utils import transform_dict_keys
from intent.checkers import IntentFeasibilityChecker
from intent.models import Context, ExpectationTarget
import logging
from random import randint
from time import sleep

from django.conf import settings
from django.db.models import Prefetch
from pika.exceptions import AMQPConnectionError

from core import celery_app
from intent.models import Intent, IntentReport, IntentExpectation, ExpectationTarget, TargetValueRange, Context, \
    ContextValueRange

LOGGER = logging.getLogger(__name__)

RETRY_OPTIONS = settings.RETRY_OPTIONS


class Command(BaseCommand):
    help = """"""

    def handle(self, *args, **options):
        intents = [
            {
                'instance_id': 'e4708935-c65b-4159-9c81-8e33524ca1e7',
                'intent_type': 'DeliverComputingWorkload',
                'service_start_time': '2024-06-21T22:00:00Z',
                'service_end_time': '2024-06-21T22:00:00Z',
                'targets': [
                    {
                        'target_name': 'cpuUsage',
                        'target_condition': 'IS_LESS_THAN',
                        'target_value_range': 20,
                    },
                    {
                        'target_name': 'ramUsage',
                        'target_condition': 'IS_LESS_THAN',
                        'target_value_range': 500,
                    }
                ]
            },
            {
                'instance_id': 'e4708935-c65b-4159-9c81-8e33524ca1e7',
                'intent_type': 'FederatedLearning',
                'targets': [
                    {
                        'target_name': 'clusterType',
                        'target_condition': 'IS_EQUAL_TO',
                        'target_value_range': 'FederatedLearning',
                    },
                ]
            }
        ]

        serializer = IntentTemplateSerializer(data=intents, many=True)
        serializer.is_valid(raise_exception=True)
        print(serializer.validated_data)
        serializer.create(serializer.validated_data)
