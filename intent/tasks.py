import json
import logging

from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder
from django.utils import timezone

from core import celery_app
from intent.checkers import IntentFeasibilityChecker
from intent.models import Intent, IntentFeasibilityReport, NotFulfilledState, ExpectationFulfilmentResult, \
    FulfilmentInfo, \
    FulfilmentStatus, TargetFulfilmentResult

LOGGER = logging.getLogger(__name__)

RETRY_OPTIONS = settings.RETRY_OPTIONS


@celery_app.task
def intent_feasibility_check(intent_id):
    try:
        from api.utils import intent_full_prefetch
        intent = Intent.objects.prefetch_related(*intent_full_prefetch).get(pk=intent_id)
    except Intent.DoesNotExist:
        LOGGER.error(f'Invalid intent_id: {intent_id}')
        return

    intent_report = intent.intent_reports.all()[0]
    intent_fulfilment_report = intent_report.intent_fulfilment_report
    intent_fulfilment_info = intent_fulfilment_report.intent_fulfilment_info

    intent_feasibility_checker = IntentFeasibilityChecker(intent)
    feasibility_check_type, infeasibility_reason = intent_feasibility_checker.check()

    # Upsert feasibility report
    if not intent_report.intent_feasibility_check_report_id:
        intent_feasibility_report = IntentFeasibilityReport.objects.create(
            feasibility_check_type=feasibility_check_type,
            infeasibility_reason=infeasibility_reason,
        )

        intent_report.intent_feasibility_check_report = intent_feasibility_report
    else:
        intent_feasibility_report = intent_report.intent_feasibility_check_report
        intent_feasibility_report.feasibility_check_type = feasibility_check_type
        intent_feasibility_report.infeasibility_reason = infeasibility_reason
        intent_feasibility_report.save()

    intent_report.last_updated_time = timezone.now()
    intent_report.save()

    match feasibility_check_type:
        case IntentFeasibilityReport.FeasibilityCheckResult.INFEASIBLE:
            intent_fulfilment_info.not_fulfilled_state = NotFulfilledState.FULFILMENTFAILED
            intent_fulfilment_info.save()
        case _:
            # Mark compliant
            intent_fulfilment_info.not_fulfilled_state = NotFulfilledState.COMPLIANT
            intent_fulfilment_info.save()

    # Instantiate expectation & target reports, if they dont exist
    expectation_fulfilment_result_qs = intent_fulfilment_report.expectation_fulfilment_results.all()
    # If intent was created (else it was updated)
    intent_created = len(expectation_fulfilment_result_qs) == 0

    if intent_created:
        intent_expectations = intent.intent_expectations.all()

        for intent_expectation in intent_expectations:
            expectation_targets = intent_expectation.expectation_targets.all()

            expectation_fulfilment_info = FulfilmentInfo.objects.create(
                fulfilment_status=FulfilmentStatus.NOT_FULFILLED,
                not_fulfilled_state=NotFulfilledState.ACKNOWLEDGED,
            )

            expectation_fulfilment_result = ExpectationFulfilmentResult.objects.create(
                intent_fulfilment_report=intent_fulfilment_report,
                expectation=intent_expectation,
                expectation_fulfilment_info=expectation_fulfilment_info
            )

            for expectation_target in expectation_targets:
                target_fulfilment_info = FulfilmentInfo.objects.create(
                    fulfilment_status=FulfilmentStatus.NOT_FULFILLED,
                    not_fulfilled_state=NotFulfilledState.ACKNOWLEDGED,
                )

                TargetFulfilmentResult.objects.create(
                    target=expectation_target,
                    expectation_fulfilment_result=expectation_fulfilment_result,
                    target_fulfilment_info=target_fulfilment_info
                )

    # Intent is Feasible, notify MO
    if feasibility_check_type == IntentFeasibilityReport.FeasibilityCheckResult.FEASIBLE and intent_created:
        from api.serializers.intent import IntentSerializer
        from nemo.tasks import publish_to_rabbitmq
        intent.refresh_from_db()
        serializer = IntentSerializer(instance=intent)
        publish_to_rabbitmq.delay(
            json.dumps(serializer.data, cls=DjangoJSONEncoder),
            settings.RABBITMQ_EXCHANGES['workload'],
            settings.RABBITMQ_ROUTING_KEYS['intent_notify']
        )