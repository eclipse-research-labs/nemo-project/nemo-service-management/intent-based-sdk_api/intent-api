import getpass

from django.core.management.base import BaseCommand, CommandError
from keycloak_integration.wrapper import KeycloakOpenIDWrapper


class Command(BaseCommand):
    help = 'Interact with Keycloak using the KeycloakOpenIDWrapper'

    def add_arguments(self, parser):
        subparsers = parser.add_subparsers(dest='command')

        # Add subparser for get_token
        parser_get_token = subparsers.add_parser('get_token')
        parser_get_token.add_argument('username', type=str, help='The username of the user')

        # Add subparser for introspect_token
        parser_introspect_token = subparsers.add_parser('introspect_token')
        parser_introspect_token.add_argument('access_token', type=str, help='The access token of the user')

        # Add subparser for get_userinfo
        parser_get_userinfo = subparsers.add_parser('get_userinfo')
        parser_get_userinfo.add_argument('access_token', type=str, help='The access token of the user')

        # Add subparser for refresh_token
        parser_refresh_token = subparsers.add_parser('refresh_token')
        parser_refresh_token.add_argument('refresh_token', type=str, help='The refresh token')

        # Add subparser for force_logout
        parser_force_logout = subparsers.add_parser('force_logout')
        parser_force_logout.add_argument('refresh_token', type=str, help='The refresh token')

    def handle(self, *args, **options):
        command = options['command']
        wrapper = KeycloakOpenIDWrapper()

        try:
            match command:
                case 'get_token':
                    username = options['username']
                    password = getpass.getpass(prompt='Password: ')
                    token = wrapper.get_token(username, password)
                    self.stdout.write(self.style.SUCCESS(f'Token: {token}'))

                case 'introspect_token':
                    access_token = options['access_token']
                    introspection = wrapper.introspect_token(access_token)
                    self.stdout.write(self.style.SUCCESS(f'Introspection: {introspection}'))

                case 'get_userinfo':
                    access_token = options['access_token']
                    userinfo = wrapper.get_userinfo(access_token)
                    self.stdout.write(self.style.SUCCESS(f'User info: {userinfo}'))

                case 'refresh_token':
                    refresh_token = options['refresh_token']
                    new_token = wrapper.refresh_token(refresh_token)
                    self.stdout.write(self.style.SUCCESS(f'New token: {new_token}'))

                case 'force_logout':
                    refresh_token = options['refresh_token']
                    response = wrapper.force_logout(refresh_token)
                    self.stdout.write(self.style.SUCCESS(f'Logout response: {response}'))

                case _:
                    raise CommandError('Invalid command')

        except Exception as e:
            raise CommandError(f'Error: {e}')
