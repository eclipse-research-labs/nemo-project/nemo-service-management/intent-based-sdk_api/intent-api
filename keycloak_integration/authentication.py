import logging
from django.contrib.auth import get_user_model
from django.db import transaction
from django.utils import timezone
from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions

from keycloak import KeycloakAuthenticationError

from nemo.constants import ROLE_CONSTANTS_MAPPING
from nemo.models import UserRole, Role
from keycloak_integration.utils import get_authentication_token, validate_authentication_token
from keycloak_integration.wrapper import KeycloakOpenIDWrapper

LOGGER = logging.getLogger(__name__)

User = get_user_model()


class KeycloakAuthentication(BaseAuthentication):
    keyword = 'Bearer'

    def authenticate(self, request):
        authentication_token = get_authentication_token(request).split()
        token = validate_authentication_token(authentication_token)
        return self.authenticate_credentials(token)

    def authenticate_credentials(self, token):
        try:
            keycloak_openid = KeycloakOpenIDWrapper()
            userinfo = keycloak_openid.get_userinfo(token)
            token_info = keycloak_openid.introspect_token(token)
            return self.create_or_update_user(token_info), None
        except KeycloakAuthenticationError as ex:
            msg = 'Keycloak Authentication Failed. Reason: {}'.format(ex)
            LOGGER.exception(msg, exc_info=True)
            raise exceptions.AuthenticationFailed(msg)
        except Exception as ex:
            msg = 'Keycloak Authentication Failed unexpected. Reason: {}'.format(ex)
            LOGGER.exception(msg, exc_info=True)
            raise exceptions.AuthenticationFailed(msg)

    def authenticate_header(self, request):
        return self.keyword

    @staticmethod
    def sync_user_roles_from_keycloak(user, keycloak_roles):
        """
        Efficiently sync the user's roles from Keycloak with the Django database.
        Uses bulk operations for improved performance.

        Args:
            user (User): Django User instance
            keycloak_roles (list): List of roles retrieved from Keycloak

        Returns:
            User: the user
        """

        # Step 1: Retrieve current roles from the Django database for the user
        existing_roles = set(user.user_roles.all().values_list('title', flat=True))

        # Step 2: Compare the roles retrieved from Keycloak
        keycloak_roles_set = set(keycloak_roles)

        # Step 3: Identify roles to add and remove
        roles_to_add = keycloak_roles_set - existing_roles
        roles_to_remove = existing_roles - keycloak_roles_set

        if roles_to_add or roles_to_remove:
            with transaction.atomic():
                LOGGER.info(f'User={user}, roles_to_add={roles_to_add}, roles_to_remove={roles_to_remove}')

                # Step 4: Bulk creation of new roles and associations
                if roles_to_add:
                    user_roles_to_create = [
                        UserRole(user=user, role_id=ROLE_CONSTANTS_MAPPING[role]) for role in roles_to_add
                    ]

                    UserRole.objects.bulk_create(user_roles_to_create)

                # Step 5: Bulk deletion of old roles
                if roles_to_remove:
                    UserRole.objects.filter(role__title__in=roles_to_remove).delete()

                user.refresh_from_db()

        return user

    def create_or_update_user(self, token_info):
        """ Register a new user or update an existing one

        Args:
            token_info (dict): The token introspection

        Returns:
            User: the user

        """
        now = timezone.now()

        with transaction.atomic():
            try:
                user = User.objects.prefetch_related('user_roles').get(username=token_info['preferred_username'])
                user.first_name = token_info['given_name']
                user.last_name = token_info['family_name']
                user.email = token_info['email']
                user.last_login = now
                user.save()
            except User.DoesNotExist:
                user = User.objects.create(
                    username=token_info['preferred_username'],
                    is_staff=False,
                    is_active=True,
                    first_name=token_info['given_name'],
                    last_name=token_info['family_name'],
                    email=token_info['email'],
                    last_login=now
                )

        # Filter roles starting with 'nemo_'
        filtered_roles = [role for role in token_info['realm_access']['roles'] if role.startswith("nemo_")]
        return self.sync_user_roles_from_keycloak(user, filtered_roles)
