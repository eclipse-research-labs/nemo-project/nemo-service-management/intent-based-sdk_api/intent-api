from django.conf import settings
from keycloak import KeycloakOpenID


class KeycloakOpenIDWrapper:
    def __init__(self, verify=True):
        self.verify = verify
        self._client = KeycloakOpenID(
            server_url=settings.KEYCLOAK_SERVER_URL,
            client_id=settings.KEYCLOAK_CLIENT_ID,
            realm_name=settings.KEYCLOAK_REALM_NAME,
            client_secret_key=settings.KEYCLOAK_CLIENT_SECRET,
            verify=self.verify
        )

    def get_token(self, username, password):
        """Retrieve the token by given user credentials.

        Args:
            username (str): The username of user
            password (str): The password of user
        """
        return self._client.token(username, password)

    def introspect_token(self, access_token):
        """Retrieve the active state of a token.

        Args:
            access_token (str): The access token of user
        """
        return self._client.introspect(access_token)

    def get_userinfo(self, access_token):
        """Retrieve the user info by given access token.

        Args:
            access_token (str): The access token of user
        """
        return self._client.userinfo(access_token)

    def refresh_token(self, refresh_token):
        """Refresh the access token by given refresh token

        Args:
            refresh_token (str): The refresh token
        """
        return self._client.refresh_token(refresh_token)

    def force_logout(self, refresh_token):
        """Force logout from the portal by given refresh token

        Args:
            refresh_token (str): The refresh token
        """
        return self._client.logout(refresh_token)
