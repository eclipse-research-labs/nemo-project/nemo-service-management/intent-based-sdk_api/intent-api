from rest_framework import HTTP_HEADER_ENCODING, exceptions


def get_authentication_token(request):
    """ Return request's 'Authorization:' header, as a bytestring.

    Returns:
        str: the token as a bytestring
    """
    authentication_token = request.META.get('HTTP_AUTHORIZATION', b'')
    if isinstance(authentication_token, str):
        return authentication_token.encode(HTTP_HEADER_ENCODING)
    return authentication_token


def validate_authentication_token(authentication_token):
    """ Validate the bearer token.

    Args:
        authentication_token (str): The original token

    Returns:
        str: the token
    """
    if len(authentication_token) == 1:
        msg = 'Invalid token header. No credentials provided.'
        raise exceptions.AuthenticationFailed(msg)
    elif len(authentication_token) > 2:
        msg = 'Invalid token header. Token string should not contain spaces.'
        raise exceptions.AuthenticationFailed(msg)

    try:
        return authentication_token[1].decode()
    except (UnicodeError, IndexError):
        msg = 'Invalid token header. Token string should not contain invalid characters.'
        raise exceptions.AuthenticationFailed(msg)