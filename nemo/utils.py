import json
import re
import uuid
from copy import deepcopy
from typing import Dict, List, Any

from django.core.serializers.json import DjangoJSONEncoder


class SemVerParser:
    def __init__(self, semver_string):
        self.semver_string = semver_string
        self.semver_regex = re.compile(
            r'^(?P<major>0|[1-9]\d*)\.'
            r'(?P<minor>0|[1-9]\d*)\.'
            r'(?P<patch>0|[1-9]\d*)'
            r'(?:-(?P<prerelease>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+'
            r'(?P<buildmetadata>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$'
        )

    def parse(self):
        match = self.semver_regex.match(self.semver_string)
        if not match:
            return ValueError(f'{self.semver_string} is not a valid Semantic Version 2 string')
        return match.groupdict()


def parse_memory_quota(memory_quota):
    pattern = re.compile(r'(\d+)([KkMmGgTtPpEe]?i?)')
    matches = pattern.findall(memory_quota)
    return [(value, unit) for value, unit in matches]


def parse_cpu_quota(cpu_quota):
    pattern = re.compile(r'(\d+)([mMkKgG]?)')
    matches = pattern.findall(cpu_quota)
    return [(value, unit) for value, unit in matches]


def convert_to_bytes(value, unit):
    units = {
        'Ki': 1024,
        'Mi': 1024 ** 2,
        'Gi': 1024 ** 3,
        'Ti': 1024 ** 4,
        'Pi': 1024 ** 5,
    }

    multiplier = units.get(unit, 1)
    return int(value) * multiplier


def convert_to_milli_cpus(value, unit):
    units = {
        'm': 1,
        'k': 1000,
        'M': 1000000,
        'G': 1000000000,
    }

    multiplier = units.get(unit, 1)
    return int(value) * multiplier


def convert_keys_to_snake_case(input_dict):
    def convert_key(key):
        # Convert camelCase to snake_case
        key = re.sub(r'([a-z0-9])([A-Z])', r'\1_\2', key)
        # Convert dash-case to snake_case
        key = key.replace("-", "_")
        return key.lower()

    def recursively_convert(value):
        if isinstance(value, dict):
            return {convert_key(k): recursively_convert(v) for k, v in value.items()}
        elif isinstance(value, list):
            return [recursively_convert(item) for item in value]
        else:
            return value

    return recursively_convert(deepcopy(input_dict))


def set_nemo_identifier(manifests: List[Dict[str, Any]], identifier: uuid.uuid4):
    for manifest in manifests:
        if 'metadata' not in manifest.keys():
            manifest['metadata'] = {}
        if 'labels' not in manifest['metadata'].keys():
            manifest['metadata']['labels'] = {}

        manifest['metadata']['labels']['nemo.eu/workload'] = str(identifier)

        # Append to Pod template spec labels when appropriate
        if manifest['kind'] in ['Deployment', 'StatefulSet', 'DaemonSet']:
            labels = manifest.get('spec', {}).get('template', {}).get('metadata', {}).get('labels', {})
            labels['nemo.eu/workload'] = str(identifier)

    return manifests

def set_linkerd_injection(manifests: List[Dict[str, Any]]):
    for manifest in manifests:
        if manifest['kind'] in ['Deployment', 'StatefulSet', 'DaemonSet']:
            annotations = manifest.get('spec', {}).get('template', {}).get('metadata', {}).get('annotations', {})
            annotations['linkerd.io/inject'] = 'enabled'
        elif manifest['kind'] in ['Pod']:
            if 'metadata' not in manifest.keys():
                manifest['metadata'] = {}
            if 'annotations' not in manifest['metadata'].keys():
                manifest['metadata']['annotations'] = {}
            manifest['metadata']['annotations']['linkerd.io/inject'] = 'enabled'

    return manifests


def serialize_model_fields(instance, fields):
    data = {field: getattr(instance, field) for field in fields}
    return json.dumps(data, cls=DjangoJSONEncoder)


def render_ingress_manifest(ingress_metadata, release_name):
    return {
        'apiVersion': 'networking.k8s.io/v1',
        'kind': 'Ingress',
        'metadata': {
            'name': release_name,
            'annotations': {
                'cert-manager.io/cluster-issuer': 'letsencrypt-prod',
                'external-dns.alpha.kubernetes.io/hostname': f'{release_name}.workload.nemo.onelab.eu',
                'konghq.com/plugins': 'keycloak'
            }
        },
        'spec': {
            'ingressClassName': 'kong',
            'rules': [
                {
                    'host': f'{release_name}.workload.nemo.onelab.eu',
                    'http': {
                        'paths': [
                            {
                                'path': ingress_metadata['ingress_path'],
                                'pathType': ingress_metadata['ingress_path_type'],
                                'backend': {
                                    'service': {
                                        'name': ingress_metadata['service_name'],
                                        'port': {
                                            'number': ingress_metadata['ingress_service_port'],
                                        }
                                    }
                                }
                            }
                        ]
                    }
                }
            ],
            'tls': [
                {
                    'hosts': [
                        f'{release_name}.workload.nemo.onelab.eu'
                    ],
                    'secretName': f'{release_name}-tls',
                }
            ]
        }
    }

def get_user_roles(user):
    return list(user.user_roles.all().values_list('id', flat=True))