from django.db import models
from django.db.models import Q, Subquery, CharField
from django.db.models.functions import Cast

from nemo.constants import RoleConstants
from nemo.models import WorkloadDocumentInstance
from nemo.utils import get_user_roles


class IntentManager(models.Manager):
    def scope(self, user):
        q = Q()
        user_roles = get_user_roles(user)

        if RoleConstants.NEMO_PROVIDER not in user_roles:
            workload_instance_qs = WorkloadDocumentInstance.objects.scope(user).annotate(
                instance_id_str=Cast('instance_id', CharField())
            ).values('instance_id_str')

            q = Q(intent_expectations__expectation_object__object_instance__in=Subquery(workload_instance_qs)) | \
                Q(intent_contexts__context_value_ranges__type_string__in=Subquery(workload_instance_qs))

        return self.filter(q)