from django.db import models
from django.db.models import Q

from nemo.constants import RoleConstants
from nemo.utils import get_user_roles


class WorkloadDocumentManager(models.Manager):
    def visible(self, user):
        q = Q()
        user_roles = get_user_roles(user)

        if RoleConstants.NEMO_PROVIDER not in user_roles:
            q = Q(user=user) | Q(scope=self.model.WorkloadDocumentScope.PUBLIC)

        return self.filter(q)

    def scope(self, user):
        q = Q()
        user_roles = get_user_roles(user)

        if RoleConstants.NEMO_PROVIDER not in user_roles:
            q = Q(user=user)

        return self.filter(q)


class WorkloadDocumentInstanceManager(models.Manager):
    def scope(self, user):
        q = Q()
        user_roles = get_user_roles(user)

        if RoleConstants.NEMO_PROVIDER not in user_roles:
            q = Q(user=user)

        return self.filter(q)