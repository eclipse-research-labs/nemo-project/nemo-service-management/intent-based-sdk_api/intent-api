import json
import logging
from collections import defaultdict
from pathlib import Path
from typing import NamedTuple, Optional, Dict, Any, List

import sh
import yaml
from asgiref.sync import async_to_sync
from pyhelm3 import Client
from pyhelm3.errors import FailedToRenderChartError, InvalidResourceError, Error as PyhelmError

from api.utils import decode_docker_config_json, decode_docker_config_auth_data, resolve_container_image, \
    skopeo_list_tags

LOGGER = logging.getLogger(__name__)


class ImagePullSecretEntry(NamedTuple):
    name: str


class DockerRegistryEntry(NamedTuple):
    registry: str
    username: str
    password: str


class DockerImageAccessValidator(NamedTuple):
    is_valid: bool
    image_mapping: Optional[Dict[str, Optional[DockerRegistryEntry]]] = None
    reason: Optional[str] = None


class ChartStructureValidator(NamedTuple):
    """Helm Chart structure validation result
    """
    is_valid: bool
    reason: Optional[str] = None
    chart_details: Optional[Dict[str, Any]] = None
    chart_values: Optional[Dict[str, Any]] = None


class ChartTemplateValidator(NamedTuple):
    """Helm Chart template validation result
    """
    is_valid: bool
    reason: Optional[str] = None
    manifests: Optional[List[Dict[str, Any]]] = None


class ContainerResourcesValidator(NamedTuple):
    is_valid: bool
    reason: Optional[str] = None
    resource_mapping: Optional[Dict[str, Any]] = None

class ServiceIngressSupportValidator(NamedTuple):
    is_valid: bool
    reason: Optional[str] = None
    ingress_metadata: Optional[Dict[str, Any]] = None


def helm_chart_structure_validator(chart_folder_path: Path) -> ChartStructureValidator:
    if not chart_folder_path.is_dir():
        return ChartStructureValidator(is_valid=False, reason='Invalid folder path')

    required_files = ['Chart.yaml', 'values.yaml']
    required_folders = ['templates']

    # Check if required files exist
    for file_name in required_files:
        file_path = chart_folder_path / file_name
        if not file_path.is_file():
            return ChartStructureValidator(is_valid=False, reason=f'Missing required file: {file_name}')

    # Check if required folder exists
    for folder_name in required_folders:
        folder_path = chart_folder_path / folder_name
        if not folder_path.is_dir():
            return ChartStructureValidator(is_valid=False, reason=f'Missing required folder: {folder_name}')

    # Check templates folder structure as well
    templates_folder_path = chart_folder_path / 'templates'
    template_files = [f for f in templates_folder_path.iterdir() if f.suffix in {'.yaml', '.tpl'}]
    if not template_files:
        return ChartStructureValidator(
            is_valid=False, reason='No .yaml or .tpl files found in templates folder')

    # Recover chart details
    chart_yaml_path = chart_folder_path / 'Chart.yaml'

    try:
        with chart_yaml_path.open('r') as file:
            chart_details = yaml.safe_load(file)
    except Exception as e:
        LOGGER.warning(f'Exception occurred while reading chart {str(e)}', exc_info=True)
        return ChartStructureValidator(is_valid=False, reason='Invalid .yaml file')

    # Recover chart values
    chart_yaml_path = chart_folder_path / 'values.yaml'

    try:
        with chart_yaml_path.open('r') as file:
            chart_values = yaml.safe_load(file)
    except Exception as e:
        LOGGER.warning(f'Exception occurred while reading chart  values{str(e)}', exc_info=True)
        return ChartStructureValidator(is_valid=False, reason='Invalid .yaml file')

    return ChartStructureValidator(is_valid=True, chart_details=chart_details, chart_values=chart_values)


def helm_chart_template_validator(chart_folder_path: Path, chart_version: int) -> ChartTemplateValidator:
    client = Client()
    chart = async_to_sync(client.get_chart)(
        chart_folder_path,
        version=chart_version
    )

    try:
        template_resources = async_to_sync(client.template_resources)(
            chart,
            'workload',
            {},
            include_crds=False
        )
        return ChartTemplateValidator(is_valid=True, manifests=list(template_resources))
    except FailedToRenderChartError as e:
        return ChartTemplateValidator(is_valid=False, reason=f'Failed to render the Chart: {str(e)}')
    except InvalidResourceError as e:
        return ChartTemplateValidator(is_valid=False, reason=f'Invalid Resource: {str(e)}')
    except PyhelmError as e:
        return ChartTemplateValidator(is_valid=False, reason=f'Helm Error: {str(e)}')


def docker_image_access_validator(manifests: List[Dict]) -> DockerImageAccessValidator:
    # Collect all docker registry secrets from the manifests
    image_pull_secrets: Dict[str, DockerRegistryEntry] = {}
    # Referenced imagePullSecrets for all container images
    container_images: Dict[str, Optional[ImagePullSecretEntry]] = {}
    # Resolved imagePullSecrets for all container images
    container_registry_mapping: Dict[str, Optional[DockerRegistryEntry]] = {}

    for manifest in manifests:
        if manifest['kind'] == 'Secret' and manifest['type'] == 'kubernetes.io/dockerconfigjson' \
                and '.dockerconfigjson' in manifest['data'].keys():
            secret_name = manifest['metadata']['name']
            secret_data = manifest['data']['.dockerconfigjson']

            try:
                secret_data_decoded = decode_docker_config_json(secret_data)

                for docker_registry, auth_data in secret_data_decoded['auths'].items():
                    username, password = decode_docker_config_auth_data(auth_data['auth'])
                    image_pull_secrets[secret_name] = DockerRegistryEntry(
                        registry=docker_registry,
                        username=username,
                        password=password
                    )
            except Exception as e:
                LOGGER.error(f'Unable to decode .dockerconfigjson. Reason: {str(e)}', exc_info=True)

    # Collect all docker images
    for manifest in manifests:
        containers = []
        spec_image_pull_secrets = None
        if manifest['kind'] in ['Deployment', 'StatefulSet', 'DaemonSet']:
            spec = manifest.get('spec', {}).get('template', {}).get('spec', {})
            containers = spec.get('containers', [])
            spec_image_pull_secrets = spec.get('imagePullSecrets', None)
        elif manifest['kind'] == 'Pod':
            spec = manifest.get('spec', {})
            containers = spec.get('containers', [])
            spec_image_pull_secrets = spec.get('imagePullSecrets', None)

        for container in containers:
            container_images[container.get('image')] = None if not spec_image_pull_secrets else \
                spec_image_pull_secrets[0]

    # Verify container images are accessible and tags exist
    for container_image, container_image_pull_secrets in container_images.items():
        container_registry_name, container_repository_name, container_image_name, container_image_tag = \
            resolve_container_image(container_image)

        container_uri = '/'.join(
            s for s in ['docker:/'] + [container_registry_name, container_repository_name, container_image_name]
            if s is not None)

        command_result = skopeo_list_tags(container_uri)
        if command_result.error is False:
            if container_image_tag in command_result.tags:
                # Image accessed & tag exists
                container_registry_mapping[container_image] = None
                continue
            return DockerImageAccessValidator(
                is_valid=False,
                reason=f'Tag={container_image_tag} not found for image={container_uri}'
            )
        else:  # Error is True
            if command_result.unauthorized is False:
                return DockerImageAccessValidator(
                    is_valid=False,
                    reason=f'Error while inspecting image={container_uri}. '
                           f'Reason: {command_result.reason}'
                )

        # At this point error=True, unauthorized=true, try to resolve docker registry credentials
        docker_registry_details = None

        if container_image_pull_secrets:
            provided_registry_name = container_image_pull_secrets['name']

            if container_image_pull_secrets and provided_registry_name in image_pull_secrets.keys():
                docker_registry_details = image_pull_secrets[provided_registry_name]
            elif container_image_pull_secrets and provided_registry_name not in image_pull_secrets.keys():
                # TODO: use internal registries
                pass
            else:
                pass

        if not docker_registry_details:
            return DockerImageAccessValidator(
                is_valid=False,
                reason=f'Cannot resolve image={container_uri}, no relevant registry credentials found'
            )

        command_result = skopeo_list_tags(
            container_uri,
            username=docker_registry_details.username,
            password=docker_registry_details.password
        )

        if command_result.error is False:
            if container_image_tag in command_result.tags:
                # Image accessed & tag exists with credentials
                container_registry_mapping[container_image] = docker_registry_details
                continue
            return DockerImageAccessValidator(
                is_valid=False,
                reason=f'Tag={container_image_tag} not found for image={container_uri} with credentials'
            )
        else:  # Error is True
            if command_result.unauthorized is False:
                return DockerImageAccessValidator(
                    is_valid=False,
                    reason=f'Error while inspecting image={container_uri} with credentials. '
                           f'Reason: {command_result.reason}'
                )
            return DockerImageAccessValidator(
                is_valid=False,
                reason=f'Authentication failure for image={container_uri} with provided credentials')

    # All container images are resolved
    return DockerImageAccessValidator(is_valid=True, image_mapping=container_registry_mapping)


def container_resources_validator(manifests: List[Dict]) -> ContainerResourcesValidator:
    invalid_containers = []
    container_resource_mapping = {}

    for manifest in manifests:
        containers = []
        if manifest['kind'] in ['Deployment', 'StatefulSet', 'DaemonSet']:
            spec = manifest.get('spec', {}).get('template', {}).get('spec', {})
            containers = spec.get('containers', [])
        elif manifest['kind'] == 'Pod':
            spec = manifest.get('spec', {})
            containers = spec.get('containers', [])

        for container in containers:
            resources = container.get('resources', {})
            limits = resources.get('limits', {})
            requests = resources.get('requests', {})

            match(limits, requests):
                case({'cpu': _, 'memory': _}, {'cpu': _, 'memory': _}):
                    container_resource_mapping[container['name']] = resources
                case _:
                    invalid_containers.append(container['name'])

    if len(invalid_containers) > 0:
        return ContainerResourcesValidator(
            is_valid=False,
            resource_mapping=container_resource_mapping,
            reason=f'Release workload must set default resources '
                   f'(limits & requests) for container(s) {",".join(invalid_containers)}'
        )

    return ContainerResourcesValidator(is_valid=True, resource_mapping=container_resource_mapping)

def service_ingress_support_validator(manifests: List[Dict]) -> ServiceIngressSupportValidator:
    ingress_metadata = []

    for manifest in manifests:
        if manifest['kind'] == 'Service':
            annotations = manifest.get('metadata', {}).get('annotations', {})

            ingress_expose = annotations.get('nemo.eu/ingress-expose', False)
            if ingress_expose:
                ingress_service_port = annotations.get('nemo.eu/ingress-service-port', None)
                ingress_path = annotations.get('nemo.eu/ingress-path', '/')
                ingress_path_type = annotations.get('nemo.eu/ingress-path-type', 'ImplementationSpecific')

                service_ports = manifest['spec'].get('ports', [])

                if ingress_service_port is None and len(service_ports) != 1:
                    return ServiceIngressSupportValidator(
                        is_valid=False,
                        reason='Service={} marked as `nemo.eu/ingress-expose: true` but has multiple ports and no '
                               '`nemo.eu/ingress-service-port` selector'.format(manifest['metadata'].get('name'))
                    )
                elif ingress_service_port is None and len(service_ports) == 1:
                    service_port = service_ports[0]
                    ingress_service_port = service_port['port']
                elif ingress_service_port not in [service_port['port'] for service_port in service_ports]:
                    return ServiceIngressSupportValidator(
                        is_valid=False,
                        reason='Service={} has `nemo.eu/ingress-service-port={}` but has not matching spec port '
                               '(spec.ports.port) value'.format(manifest['metadata'].get('name'), ingress_service_port)
                    )

                ingress_metadata.append({
                    'service_name': manifest['metadata']['name'],
                    'ingress_expose': ingress_expose,
                    'ingress_path': ingress_path,
                    'ingress_path_type': ingress_path_type,
                    'ingress_service_port': ingress_service_port
                })

    match len(ingress_metadata):
        case 0:
            # TODO: This may change in order to allow ingress enabling optionally during workload rendering
            return ServiceIngressSupportValidator(
                is_valid=False,
                reason=f'Release workload has no Service marked as `nemo.eu/ingress-expose: true`'
            )
        case 1:
            return ServiceIngressSupportValidator(
                is_valid=True,
                ingress_metadata=ingress_metadata[0],
            )
        case _:
            return ServiceIngressSupportValidator(
                is_valid=False,
                reason=f'Release workload must have only one Service marked as `nemo.eu/ingress-expose: true`'
            )





