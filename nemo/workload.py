import logging

from nemo.models import WorkloadDocumentInstance, WorkloadDocumentLifecycleEvent

LOG = logging.getLogger(__name__)

class WorkloadLifecycleManager:
    """MO integration
    """
    def parse_lifecycle_message(self, message):
        LOG.info('Parsing lifecycle event message: %s', message)

        message_type = message['type']
        instance_id = message['workloadID']
        timestamp = message['timestamp']

        try:
            workload_document_instance = WorkloadDocumentInstance.objects.get(instance_id=instance_id)
        except WorkloadDocumentInstance.DoesNotExist:
            LOG.error('Invalid workload document instance id={}'.format(instance_id))
            return

        match message_type:
            case 'deployment':
                WorkloadDocumentLifecycleEvent.objects.create(
                    workload_document_instance=workload_document_instance,
                    type=WorkloadDocumentLifecycleEvent.WorkloadDocumentLifecycleType.DEPLOYMENT,
                    deployment_cluster=message['targetCluster'],
                    timestamp=timestamp,
                )

                workload_document_instance.cluster_name = message['targetCluster']
                workload_document_instance.status = WorkloadDocumentInstance.WorkloadDocumentInstanceStatus.DEPLOYED
                workload_document_instance.save()
            case 'migration':
                WorkloadDocumentLifecycleEvent.objects.create(
                    workload_document_instance=workload_document_instance,
                    type=WorkloadDocumentLifecycleEvent.WorkloadDocumentLifecycleType.MIGRATION,
                    migration_from_cluster=message['fromCluster'],
                    migration_to_cluster=message['toCluster'],
                    timestamp=timestamp,
                )

                workload_document_instance.cluster_name = message['toCluster']
                workload_document_instance.save()
            case 'delete':
                WorkloadDocumentLifecycleEvent.objects.create(
                    workload_document_instance=workload_document_instance,
                    type=WorkloadDocumentLifecycleEvent.WorkloadDocumentLifecycleType.DELETION,
                    deployment_cluster=message['targetCluster'],
                    timestamp=timestamp,
                )

                workload_document_instance.cluster_name = message['toCluster']
                workload_document_instance.status = WorkloadDocumentInstance.WorkloadDocumentInstanceStatus.DELETED
                workload_document_instance.save()
            case _:
                LOG.warning('Got message_type: %s', message_type)
                return