from django.contrib import admin
from django.urls import reverse
from django.utils.html import format_html

from nemo.models import WorkloadDocument, WorkloadDocumentChart, WorkloadDocumentChartMaintainer, \
    WorkloadDocumentChartDependency, WorkloadDocumentChartMetadata
from nemo.models.auth import Role, User, UserRole
from nemo.models.workload import WorkloadDocumentInstance


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    pass


@admin.register(Role)
class RoleAdmin(admin.ModelAdmin):
    pass


@admin.register(UserRole)
class UserRoleAdmin(admin.ModelAdmin):
    pass


@admin.register(WorkloadDocument)
class WorkloadDocumentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'version', 'type', 'get_chart_link', 'status', 'modified', 'created')
    list_filter = ('status', 'type')
    search_fields = ('name', )

    def get_chart_link(self, obj):
        if obj.chart:
            link = reverse('admin:nemo_workloaddocumentchart_change', args=[obj.chart.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        else:
            return None

    get_chart_link.short_description = 'Chart'


class WorkloadDocumentChartMaintainerInline(admin.TabularInline):
    model = WorkloadDocumentChartMaintainer


class WorkloadDocumentChartDependencyInline(admin.TabularInline):
    model = WorkloadDocumentChartDependency


@admin.register(WorkloadDocumentChart)
class WorkloadDocumentChartAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'version', 'app_version', 'api_version', 'kube_version', 'type',)
    list_filter = ('api_version', 'kube_version', 'type')
    search_fields = ('name', )
    inlines = [WorkloadDocumentChartMaintainerInline, WorkloadDocumentChartDependencyInline]


@admin.register(WorkloadDocumentChartMetadata)
class WorkloadDocumentChartMetadataAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_chart_link', 'memory_requests', 'memory_limits', 'cpu_requests', 'cpu_limits')

    def get_chart_link(self, obj):
        if obj.chart:
            link = reverse('admin:nemo_workloaddocumentchart_change', args=[obj.chart.id])
            return format_html('<a href="{}">{}</a>', link, obj)
        else:
            return None

    get_chart_link.short_description = 'Chart'


@admin.register(WorkloadDocumentInstance)
class WorkloadDocumentInstanceAdmin(admin.ModelAdmin):
    readonly_fields = ('instance_id', )