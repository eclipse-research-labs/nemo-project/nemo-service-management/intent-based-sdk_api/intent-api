import logging
from random import randint
from time import sleep

from django.conf import settings
from pika.exceptions import AMQPConnectionError

from core import celery_app

from nemo.models import WorkloadDocument
from nemo.rabbitmq import RabbitMQClient

LOGGER = logging.getLogger(__name__)

RETRY_OPTIONS = settings.RETRY_OPTIONS


@celery_app.task
def post_process_workload_document(workload_document_id):
    try:
        workload_document = WorkloadDocument.objects.get(pk=workload_document_id)
    except WorkloadDocument.DoesNotExist:
        LOGGER.error(f'Invalid workload_document_id: {workload_document_id}')
        return

    # TODO post processing steps
    sleep(randint(10, 20))
    LOGGER.debug(f'Workload Document {workload_document} passed v&v validation')
    workload_document.status = WorkloadDocument.WorkloadDocumentStatus.ACCEPTED
    workload_document.save()

    # Notify Rabbitmq when document is accepted
    publish_to_rabbitmq.delay(
        workload_document.serialize(),
        settings.RABBITMQ_EXCHANGES['workload'],
        settings.RABBITMQ_ROUTING_KEYS['workload_upload']
    )


@celery_app.task(autoretry_for=(AMQPConnectionError,), **RETRY_OPTIONS)
def publish_to_rabbitmq(message, exchange, routing_key):
    client = RabbitMQClient()
    client.publish(message, exchange, routing_key)