
from django.core.exceptions import ValidationError
from django.db import models

from nemo.utils import SemVerParser


def validate_semver(value):
    try:
        parser = SemVerParser(value)
        parser.parse()
    except ValueError as e:
        raise ValidationError(str(e))


class SemVer2Field(models.CharField):
    default_validators = [validate_semver]

    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 32)
        super().__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super().deconstruct()
        return name, path, args, kwargs

    def validate_semver(self, value):
        try:
            parser = SemVerParser(value)
            parser.parse()
        except ValueError as e:
            raise ValidationError(str(e))
