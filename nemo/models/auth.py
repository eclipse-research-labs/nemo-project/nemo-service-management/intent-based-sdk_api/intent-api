from django.contrib.auth.models import AbstractUser
from django.db import models
from model_utils.models import TimeStampedModel

from nemo.apps import NemoConfig


class Role(models.Model):
    title = models.CharField(max_length=50, unique=True, db_index=True, help_text='NEMO role')
    description = models.TextField(blank=True, null=True, help_text='NEMO role description')
    active = models.BooleanField(default=True, db_index=True, help_text='Nemo role status')

    class Meta:
        app_label = NemoConfig.name
        db_table = 'nemo_role'
        verbose_name = 'Role'
        verbose_name_plural = 'Roles'

    def __str__(self):
        return self.title


class User(AbstractUser):
    user_roles = models.ManyToManyField(
        Role,
        through='UserRole',
        through_fields=('user', 'role')
    )

    class Meta:
        app_label = NemoConfig.name
        db_table = 'nemo_user'
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class UserRole(TimeStampedModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    role = models.ForeignKey(Role, on_delete=models.CASCADE)

    class Meta:
        app_label = NemoConfig.name
        db_table = 'nemo_user_role'
        verbose_name = 'User Role'
        verbose_name_plural = 'User Roles'

    def __str__(self):
        return f'{self.user} - {self.role}'