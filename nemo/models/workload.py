import uuid

from django.conf import settings
from django.contrib.postgres.fields import ArrayField, HStoreField
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from model_utils import Choices
from model_utils.models import TimeStampedModel

from nemo.apps import NemoConfig
from nemo.fields import SemVer2Field
from nemo.managers.workload import WorkloadDocumentManager, WorkloadDocumentInstanceManager
from nemo.utils import serialize_model_fields


class WorkloadDocumentChartMetadata(models.Model):
    chart = models.OneToOneField('WorkloadDocumentChart', on_delete=models.CASCADE, related_name='metadata')
    container_registries = models.JSONField(help_text='The mapped container registries')
    chart_details = models.JSONField(help_text='The mapped chart details')
    manifests = ArrayField(models.JSONField(), help_text='The default generated manifests')
    values = models.JSONField(help_text='The chart default values')
    resource_mappings = models.JSONField(blank=True, null=True, help_text='The container resource mappings')
    memory_requests = models.BigIntegerField(blank=True, null=True, help_text='Total memory requests (in bytes)')
    memory_limits = models.BigIntegerField(blank=True, null=True, help_text='Total memory limits (in bytes)')
    cpu_requests = models.IntegerField(blank=True, null=True, help_text='Total CPU requests (in milli cpus')
    cpu_limits = models.IntegerField(blank=True, null=True, help_text='Total CPU limits (in milli cpus)')

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_chart_metadata'
        verbose_name = 'Workload Document Chart Metadata'
        verbose_name_plural = 'Workload Document Chart Metadata'

    def __str__(self):
        return str(self.chart)


class WorkloadDocumentChartMaintainer(models.Model):
    """Helm Chart Maintainer associated with the Workload Document Helm Chart

    https://helm.sh/docs/topics/charts/#the-chartyaml-file
    """
    chart = models.ForeignKey('WorkloadDocumentChart', on_delete=models.CASCADE, related_name='maintainers')
    name = models.CharField(max_length=100, help_text='The maintainers name (required)')
    email = models.EmailField(help_text='The maintainers email (optional)', blank=True, null=True)
    url = models.URLField(help_text='A url for the maintainer (optional)', blank=True, null=True)

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_chart_maintainer'
        verbose_name = 'Workload Document Chart Maintainer'
        verbose_name_plural = 'Workload Document Chart Maintainers'

    def __str__(self):
        return self.name


class WorkloadDocumentChartDependency(models.Model):
    """Helm Chart Dependency associated with the Workload Document Helm Chart

    https://helm.sh/docs/topics/charts/#the-chartyaml-file
    """
    chart = models.ForeignKey('WorkloadDocumentChart', on_delete=models.CASCADE, related_name='dependencies')
    name = models.CharField(max_length=100, help_text='The name of the chart')
    version = SemVer2Field(help_text='A SemVer 2 version string')
    repository = models.CharField(max_length=100, help_text='The repository URL or alias ("repo-name") (optional)', blank=True, null=True)
    condition = models.CharField(max_length=50, help_text='A yaml path that resolves to a boolean, used for enabling/disabling charts (e.g. subchart1.enabled) (optional)', blank=True, null=True)
    tags = ArrayField(models.CharField(max_length=20), help_text='Tags can be used to group charts for enabling/disabling together', blank=True, null=True)
    import_values = ArrayField(models.CharField(max_length=100), help_text='ImportValues holds the mapping of source values to parent key to be imported.', blank=True, null=True)
    alias = models.CharField(max_length=20, help_text='Alias to be used for the chart. Useful when you have to add the same chart multiple times', blank=True, null=True)


    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_chart_dependency'
        verbose_name = 'Workload Document Chart Dependency'
        verbose_name_plural = 'Workload Document Chart Dependencies'

    def __str__(self):
        return f'{self.name} v{self.version}'


class WorkloadDocumentChart(models.Model):
    """Helm Chart associated with the Workload Document

    https://helm.sh/docs/topics/charts/#the-chartyaml-file
    """
    api_version = models.CharField(max_length=2, help_text='The chart API version (required)')
    name = models.CharField(max_length=100, help_text='The name of the chart (required)', db_index=True)
    version = SemVer2Field(help_text='A SemVer 2 version string', db_index=True)
    kube_version = models.CharField(max_length=20, help_text='A SemVer range of compatible Kubernetes versions (optional)', blank=True, null=True)
    description = models.TextField(help_text='A single-sentence description of this project (optional)', blank=True, null=True)
    type = models.CharField(max_length=20, help_text='The type of the chart (optional)', blank=True, null=True)
    keywords = ArrayField(models.CharField(max_length=20), help_text='A list of keywords about this project (optional)', blank=True, null=True)
    home = models.URLField(help_text='The URL of this projects home page (optional)', blank=True, null=True)
    sources = ArrayField(models.URLField(), help_text='A list of URLs to source code for this project (optional)', blank=True, null=True)
    icon = models.URLField(help_text='A URL to an SVG or PNG image to be used as an icon (optional)', blank=True, null=True)
    app_version = models.CharField(max_length=20, help_text='The version of the app that this contains (optional). Needn\'t be SemVer. Quotes recommended.', blank=True, null=True)
    deprecated = models.BooleanField(help_text='Whether this chart is deprecated (optional, boolean)', blank=True, null=True)
    annotations = HStoreField(help_text='A list of annotations keyed by name (optional)', blank=True, null=True)

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_chart'
        verbose_name = 'Workload Document Chart'
        verbose_name_plural = 'Workload Document Charts'
        unique_together = ['name', 'version']

    def __str__(self):
        return f'{self.name} v{self.version}'


class WorkloadDocument(TimeStampedModel):
    """NEMO workload document
    """
    class WorkloadDocumentType(models.TextChoices):
        CHART = 'chart', 'Helm Chart'

    class WorkloadDocumentStatus(models.TextChoices):
        PENDING = 'pending', 'Pending Upload'
        ONBOARDING = 'onboarding', 'Onboarding (running validation evaluation)'
        ACCEPTED = 'accepted', 'Document accepted for usage'
        REJECTED = 'rejected', 'Document rejected (e.g. validation failed)'

    class WorkloadDocumentScope(models.TextChoices):
        PUBLIC = 'public', 'Public'
        PRIVATE = 'private', 'Private'


    chart = models.OneToOneField(WorkloadDocumentChart, on_delete=models.CASCADE, related_name='document', help_text='The associated helm chart', blank=True, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='workload_documents', help_text='The NEMO user')
    name = models.CharField(max_length=100, help_text='The workload document name', db_index=True)
    version = SemVer2Field(help_text='A SemVer 2 version string', db_index=True)
    schema = models.JSONField(help_text='The document schema')
    intents = ArrayField(models.CharField(max_length=100, help_text='Intent userLabel'), help_text='List of supported intents', default=list)
    type = models.CharField(choices=WorkloadDocumentType, default=WorkloadDocumentType.CHART, db_index=True, help_text='The workload document type')
    status = models.CharField(choices=WorkloadDocumentStatus, default=WorkloadDocumentStatus.PENDING, db_index=True, help_text='The workload document status')
    scope = models.CharField(choices=WorkloadDocumentScope, default=WorkloadDocumentScope.PUBLIC, db_index=True, help_text='The workload document scope')
    ingress_support = models.BooleanField(default=False, db_index=True, help_text='Whether the workload document can be exposed via NEMO')
    enabled = models.BooleanField(default=True, help_text='If the workload document is enabled')
    rejection_reason = models.TextField(help_text='Rejection reason', blank=True, null=True)

    objects = WorkloadDocumentManager()

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document'
        verbose_name = 'Workload Document'
        verbose_name_plural = 'Workload Documents'
        unique_together = ['name', 'version']

    def __str__(self):
        return f'{self.name} v{self.version}'

    def serialize(self):
        return serialize_model_fields(self, ('id', 'name', 'version', 'type', 'status'))


class WorkloadDocumentInstance(TimeStampedModel):
    """Nemo Workload Document instance
    """
    class WorkloadDocumentInstanceStatus(models.TextChoices):
        RENDERED = 'rendered', 'Rendered'
        DEPLOYED = 'deployed', 'Deployed'
        MIGRATING = 'migrating', 'Migrating'
        EVICTED = 'evicted', 'Evicted'
        DELETED = 'deleted', 'Deleted'

    instance_id = models.UUIDField(default=uuid.uuid4, unique=True, db_index=True, editable=False, help_text='NEMO unique workload document instance identifier')
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='workload_document_instances', help_text='The NEMO user', blank=True, null=True)
    workload_document = models.ForeignKey(WorkloadDocument, on_delete=models.CASCADE, related_name='instances', help_text='The workload document')
    release_name = models.CharField(max_length=100, unique=True, db_index=True, help_text='The workload document instance release name')
    status = models.CharField(choices=WorkloadDocumentInstanceStatus, default=WorkloadDocumentInstanceStatus.RENDERED, db_index=True, help_text='The workload document instance status')
    manifests = ArrayField(models.JSONField(), default=list, help_text='The generated instance manifests')
    input_metadata = models.JSONField(encoder=DjangoJSONEncoder, help_text='The generated instance input metadata')
    lifecycle_metadata = ArrayField(models.JSONField(), blank=True, null=True, help_text='The lifecycle metadata associated with the instance')
    cluster_name = models.CharField(max_length=100, db_index=True, blank=True, null=True, help_text='The NEMO cluster name that the instance resides in')
    ingress_enabled = models.BooleanField(default=False, db_index=True, help_text='Whether the instance should be exposed via NEMO')
    ingress_metadata = models.JSONField(blank=True, null=True, help_text='Ingress metadata')

    objects = WorkloadDocumentInstanceManager()

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_instance'
        verbose_name = 'Workload Document Instance'
        verbose_name_plural = 'Workload Document Instances'

    def __str__(self):
        return f'{self.release_name} ({str(self.workload_document)})'

    def serialize(self):
        return serialize_model_fields(self, (
            'id', 'instance_id', 'workload_document_id', 'release_name', 'status', 'manifests', 'cluster_name'))


class WorkloadDocumentLifecycleEvent(models.Model):
    class WorkloadDocumentLifecycleType(models.TextChoices):
        RENDERED = 'rendered', 'Rendered'
        DEPLOYMENT = 'deployment', 'Deployment'
        MIGRATION = 'migration', 'Migration'
        EVICTION = 'eviction', 'Eviction'
        DELETION = 'deletion', 'Deletion'

    workload_document_instance = models.ForeignKey(WorkloadDocumentInstance, on_delete=models.CASCADE, related_name='lifecycle_events', help_text='The workload document instance lifecycle events')
    type = models.CharField(choices=WorkloadDocumentLifecycleType, db_index=True, help_text='Lifecycle event type')
    deployment_cluster = models.CharField(max_length=100, blank=True, null=True, help_text='The NEMO deployment cluster')
    migration_from_cluster = models.CharField(max_length=100, blank=True, null=True, help_text='The NEMO migration from cluster')
    migration_to_cluster = models.CharField(max_length=100, blank=True, null=True, help_text='The NEMO migration to cluster')
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True, help_text='The timestamp of the lifecycle event')

    class Meta:
        app_label = NemoConfig.name
        db_table = 'workload_document_lifecycle_event'
        verbose_name = 'Workload Document Instance Lifecycle Event'
        verbose_name_plural = 'Workload Document Instance Lifecycle Events'

    def __str__(self):
        return f'{self.workload_document_instance.release_name} ({self.type})'
















