from django.db import models


class RoleConstants(models.IntegerChoices):
    # keycloak_role = id, title
    NEMO_PROVIDER = 1, 'nemo_provider'
    NEMO_CONSUMER = 2, 'nemo_consumer'
    NEMO_PARTNER = 3, 'nemo_partner'


ROLE_CONSTANTS_MAPPING = {role.name.lower(): role for role in RoleConstants}


class IntentUserLabels(models.TextChoices):
    CLOUD_CONTINUUM = 'cloud_continuum', 'cloud_continuum'
    DELIVER_COMPUTING_WORKLOAD = 'DeliverComputingWorkload', 'DeliverComputingWorkload'
    ENERGY_CARBON_EFFICIENCY = 'EnergyCarbonEfficiency', 'EnergyCarbonEfficiency'
    FEDERATED_LEARNING = 'FederatedLearning', 'FederatedLearning'
    SECURE_EXECUTION = 'SecureExecution', 'SecureExecution'
    AVAILABILITY = 'Availability', 'Availability'
