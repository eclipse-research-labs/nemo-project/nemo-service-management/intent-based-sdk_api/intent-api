import logging
from contextlib import contextmanager
from typing import Optional, List, Callable

import pika
from django.conf import settings
from pika import BasicProperties
from pika.channel import Channel
from pika.spec import Basic

LOGGER = logging.getLogger(__name__)

CallbackType = Callable[[Channel, Basic.Deliver, BasicProperties, bytes], None]


class RabbitMQClient:
    """
    RabbitMQ client simple wrapper
    """
    def __init__(self, host: Optional[str] = None, port: Optional[int] = None, user: Optional[str] = None,
                 password: Optional[str] = None):
        self.host = host if host is not None else settings.RABBITMQ_HOST
        self.port = port if port is not None else settings.RABBITMQ_PORT
        self.user = user if user is not None else settings.RABBITMQ_USER
        self._password = password if password is not None else settings.RABBITMQ_PASSWORD

    @contextmanager
    def connection(self):
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=self.host, port=self.port,
                credentials=pika.PlainCredentials(self.user, self._password)
            )
        )
        try:
            yield connection
        finally:
            connection.close()

    def publish(self, message: str, exchange: str, routing_key: str):
        with self.connection() as connection:
            channel = connection.channel()
            channel.basic_publish(
                exchange=exchange,
                routing_key=routing_key,
                body=message
            )

            LOGGER.info(f"[x] Sent to {exchange}, routing_key={routing_key}, message:\n{message}")

    def subscribe(self, exchange: Optional[str], routing_keys: Optional[List[str]], callback_fn: CallbackType, queue: Optional[str] = '', durable: bool = False):
        with self.connection() as connection:
            channel = connection.channel()
            result = channel.queue_declare(queue=queue, durable=durable)
            queue_name = result.method.queue

            routing_keys = [] if routing_keys is None else routing_keys

            for routing_key in routing_keys:
                channel.queue_bind(exchange=exchange, queue=queue_name, routing_key=routing_key)

            LOGGER.info(f"[x] Waiting for logs queue={queue_name}, routing_keys={routing_keys}, exchange={exchange}")

            channel.basic_consume(
                queue=queue_name, on_message_callback=callback_fn, auto_ack=True)

            channel.start_consuming()
