import json

import pika
from django.core.management import BaseCommand

from nemo.rabbitmq import RabbitMQClient


class Command(BaseCommand):
    help = 'Subscribe to rabbitmq'

    def add_arguments(self, parser):
        parser.add_argument('--exchange', type=str, required=False, help='Specify exchange name', )
        parser.add_argument('--routing-keys', nargs='+', type=str, required=False, help='Specify routing keys')
        parser.add_argument('--queue', type=str, help='Specify queue name')
        parser.add_argument('--durable', action='store_true', help='Specify if queue is durable')

    def handle(self, *args, **options):
        exchange = options.get('exchange', None)
        routing_keys = options.get('routing_keys', None)
        queue = options['queue']
        durable = options['durable']

        def callback(ch, method, properties, body):
            print(f" [x] {method.routing_key}:{json.loads(body)}")

        client = RabbitMQClient()
        client.subscribe(exchange=exchange, routing_keys=routing_keys, callback_fn=callback, queue=queue, durable=durable)