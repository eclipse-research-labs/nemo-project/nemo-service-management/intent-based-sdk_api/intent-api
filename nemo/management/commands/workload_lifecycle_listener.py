import json

from django.conf import settings
from django.core.management import BaseCommand

from nemo.rabbitmq import RabbitMQClient
from nemo.workload import WorkloadLifecycleManager


class Command(BaseCommand):
    help = 'Subscribe to rabbitmq Workload update messages from MO'

    def add_arguments(self, parser):
        parser.add_argument('--queue', type=str, help='Specify queue name')
        parser.add_argument('--durable', action='store_true', help='Specify if queue is durable')

    def handle(self, *args, **options):
        exchange = settings.RABBITMQ_EXCHANGES['workload']
        routing_keys = [settings.RABBITMQ_ROUTING_KEYS['workload_update']]
        queue = options['queue']
        durable = options['durable']

        def callback(ch, method, properties, body):
            message = json.loads(body)
            manager = WorkloadLifecycleManager()
            manager.parse_lifecycle_message(message)

        client = RabbitMQClient()
        client.subscribe(exchange=exchange, routing_keys=routing_keys, callback_fn=callback, queue=queue, durable=durable)