import json

import pika
from django.core.management import BaseCommand

from nemo.rabbitmq import RabbitMQClient
import yaml


class Command(BaseCommand):
    help = 'Publish to rabbitmq'

    def handle(self, *args, **options):
        client = RabbitMQClient()

        # payload = {
        #     'Intents': [
        #         {
        #             'id': 26,
        #             'fullfilled': True,
        #             'intentExpectations': [
        #                 {'expectationId': 22, 'fulfilled': True, 'expectationTargets': [
        #                     {'targetId': 55, 'fulfilled': True, 'target_achieved_value': 0.075},
        #                     {'targetId': 56, 'fulfilled': True, 'target_achieved_value': 10}
        #                 ]}
        #             ]}
        #     ]
        # }

        payload = {
            'workloadID': '0f552ef7-0ce6-4995-86ee-589039c9d27a',
            'type': 'deployment',
            'deploymentCluster': 'test_cluster',
            'timestamp': '2024-09-12T09:34:21.747620Z'
        }

        client.publish(json.dumps(payload), exchange='nemo.api.workload', routing_key='update')