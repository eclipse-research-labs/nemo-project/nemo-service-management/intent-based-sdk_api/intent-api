from asgiref.sync import async_to_sync
from django.core.management import BaseCommand
from pydantic._internal._generics import get_args
from pydantic.v1.typing import WithArgsTypes
from pydantic.v1.utils import lenient_issubclass


class Command(BaseCommand):
    help = """"""

    def handle(self, *args, **options):
        from pyhelm3 import Client

        from pydantic_core._pydantic_core import Url
        # This will use the Kubernetes configuration from the environment
        client = Client()

        # List the deployed releases
        releases = async_to_sync(client.list_releases)(all=True, all_namespaces=True)
        for release in releases:
            revision = async_to_sync(release.current_revision)()
            print(release.name, release.namespace, revision.revision, str(revision.status))

        #Get the current revision for an existing release
        revision = async_to_sync(client.get_current_revision)("akhq", namespace="sentry")
        chart_metadata = async_to_sync(revision.chart_metadata)()
        print(
            revision.release.name,
            revision.release.namespace,
            revision.revision,
            str(revision.status),
            chart_metadata.name,
            chart_metadata.version
        )

        # Fetch a chart
        chart = async_to_sync(client.get_chart)(
            "cert-manager",
            repo="https://charts.jetstack.io",
            version="v1.8.x"
        )
        print(chart.metadata.name, chart.metadata.version)
        #print(async_to_sync(chart.readme)())
        values = async_to_sync(client.template_resources)(
            chart,
            'mycertmanager',
            {'replicaCount': 2},
            include_crds=False
        )
        for value in values:
            print('-------------------')
            print(value)
        #Fetch a chart
        chart = async_to_sync(client.get_chart)(
            "echo-server",
            repo="s3://nemo-helm-repository-37db8f4f-da53-4e2f-bde1-3f153b582433/charts",
            version="0.5.1"
        )
        print(chart.metadata.name, chart.metadata.version)
        #print(async_to_sync(chart.readme)())
        values = async_to_sync(client.template_resources)(
            chart,
            'echo-server',
            {},
            include_crds=False
        )
        for value in values:
            print('-------------------')
            print(value)

        chart = async_to_sync(client.get_chart)(
            "oci://registry-1.docker.io/bitnamicharts/postgresql",
            version="13.4.1"
        )
        print(chart.metadata.name, chart.metadata.version)
        async_to_sync(chart.readme)()
        values = async_to_sync(client.template_resources)(
            chart,
            'postgresql',
            {},
            include_crds=False
        )
        for value in values:
            print('-------------------')
            print(value)