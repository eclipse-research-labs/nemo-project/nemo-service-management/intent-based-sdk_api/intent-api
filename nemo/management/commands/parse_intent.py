import yaml
from django.core.management import BaseCommand

from api.serializers.intent import IntentInputSerializer, ExpectationTargetSerializer, ContextSerializer, \
    IntentInputAttributeSerializer
from api.utils import transform_dict_keys
from intent.checkers import IntentFeasibilityChecker
from intent.models import Context, ExpectationTarget
import logging
from random import randint
from time import sleep

from django.conf import settings
from django.db.models import Prefetch
from pika.exceptions import AMQPConnectionError

from core import celery_app
from intent.models import Intent, IntentReport, IntentExpectation, ExpectationTarget, TargetValueRange, Context, \
    ContextValueRange

LOGGER = logging.getLogger(__name__)

RETRY_OPTIONS = settings.RETRY_OPTIONS


class Command(BaseCommand):
    help = """"""

    def handle(self, *args, **options):
        with open('network-intents.yaml') as file:
            data = list(yaml.safe_load_all(file))

        doc = data[0]

        intent_serializer = IntentInputAttributeSerializer(data=transform_dict_keys(doc))
        intent_serializer.is_valid(raise_exception=True)
        validated_data = intent_serializer.validated_data
        print(validated_data)
        # intent = intent_serializer.create(validated_data)
        # print(intent.pk)
        #
        # context = Context.objects.get(id=10)
        # #print(context.context_value_ranges.all())
        # serializer = ContextSerializer(instance=context)
        # print(serializer.data)

        # et = ExpectationTarget.objects.get(id=13)
        # print(vars(et))
        #
        # print(ExpectationTargetSerializer(instance=et).data)
        # intent = Intent.objects.prefetch_related(
        #     Prefetch(
        #         'intent_reports',
        #         queryset=IntentReport.objects.select_related('intent_fulfilment_report__intent_fulfilment_info')
        #     ),
        #     Prefetch(
        #         'intent_expectations',
        #         queryset=IntentExpectation.objects.select_related('expectation_object').prefetch_related(
        #             Prefetch(
        #                 'expectation_targets',
        #                 queryset=ExpectationTarget.objects.prefetch_related(
        #                     Prefetch(
        #                         'target_value_ranges',
        #                         queryset=TargetValueRange.objects.select_related('type_time_window')
        #                     )
        #                 )
        #             ),
        #             Prefetch(
        #                 'expectation_object__object_contexts',
        #                 queryset=Context.objects.prefetch_related(
        #                     Prefetch(
        #                         'context_value_ranges',
        #                         queryset=ContextValueRange.objects.select_related('type_time_window')
        #                     )
        #                 )
        #             )
        #         )
        #     )
        # ).all().last()
        #
        # intent_feasibility_checker = IntentFeasibilityChecker(intent)
        # feasibility_check_type, infeasibility_reason = intent_feasibility_checker.check()
        # print(feasibility_check_type, infeasibility_reason)

