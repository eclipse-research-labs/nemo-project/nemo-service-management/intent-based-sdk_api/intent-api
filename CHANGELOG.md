Intent API Changelog
=

v0.1.0 (22/03/2024)
==
- Added preliminary RabbitMQ integration ( see README.md)
- Updated endpoint ```/workload/upload``` to return code=201 upon successful upload

v0.1.1 (22/03/2024)
==
- Added missing pika library

v0.1.2 (09/04/2024)
==
- Append NEMO nemo.eu/workload label to Deployment/Statefulset/Daemonset pod template spec

v0.1.3 (17/04/2024)
==

- Added MOCA integrations endpoints :
    * /cluster/  ~  POST
    * /cluster/retrieve/  ~  GET
    * /cluster/retrieve/<uuid>/  ~  GET

v0.2.0 (26/06/2024)
==

- Added Intent integration endpoints & models :
    * /intent/ ~ POST
    * /intent/ ~ GET

v0.2.1 (2/07/2024)
==

- Added keycloak integration

v0.2.2 (5/07/2024)```````````
==

- Added mncc integration


v0.2.3 (9/07/2024)
==

- Added endpoints:
  * ```/intent/types/``` ~ GET (parametric) get valid intent types (userLabels)
  * ```/intent/template/``` ~ POST create template for given instance on NEMO Workload

- Updated endpoints:
  * ```/workload/``` ~ POST added value named ```intents``` that is an array of valid intent types (userLabels) that the NEMO Workload will support
  * ```/workload/{id}/``` ~ PUT,PATCH in order to allow ```intents``` changes


v0.2.4 (18/07/2024)
==

Added Basic Auth to ```/intent/``` endpoint

v0.2.5 (23/07/2024)
==

- Changed ```/intent/``` endpoint auth priority
- Added ```/workload/instance/<uuid:instance>/manifests/``` ~ GET fetch manifests of given workload instance in YAML format

v0.2.6 (24/07/2024)
==

- Notify rabbitmq when a valid intent is created or updated

v0.2.7 (24/07/2024)
==

- Added intent collector command stub

v0.2.13 (24/07/2024)
==

- Added intent collector logic

v0.2.14 (29/07/2024)
==

- Fixed typo in intent collector
- Added ```/intent/<int:pk>/action/``` ~ PUT endpoint in order to update an intent

v0.2.15 (30/07/2024)
==

- Added ```/intent/<int:pk>/target/``` ~ PUT endpoint in order to update an intent target

v0.2.16 (12/09/2024) 
==

- Added migrations for better workload lifecycle management
- ```/workload/instance/``` returns the lifecycle events of the workload
- Added lifecycle event listener command (MO Integration)

v0.2.17 (17/09/2024)
==

- Added CORS Headers package
- Update django settings


v0.2.18 (3/10/2024)
==

- Updated ```/workload/```, added ingress_support boolean value
- Updated ```/workload/{id}/template/```, added ingress_enabled boolean value
- Support for the following service annotations (CHECK README.md)
  * nemo.eu/ingress-expose
  * nemo.eu/ingress-service-port
  * nemo.eu/ingress-path
  * nemo.eu/ingress-path-type

v0.2.19 (4/10/2024)
==

- Added optional parameter ```cluster_name``` during workload instantiation (```/workload/{id}/template/```)

v0.2.20 (10/10/2024)
==

- Added endpoint ```/workload/instance/<uuid>/delete/``` ~ PUT for workload instance deletion

v0.2.21 (10/10/2024)
==

- 

v0.2.22
==

- Added ```intents``` field to ```/workload/{workload_id)/template/``` in order to allow to POST an intent template along with the workload instantiation
- Intents created during workload instantiation are attached as payload to ```nemo.api.workload```, ```run``` (exchange, routing_key) rabbitmq message
- Added user field in workload instance model
- Added scope field (private, public) in workloads
- Scope workloads documents, workload document instance, intents per related user (nemo_consumer), nemo_provider user has admin access 
- Added role resolution (permissions) in API endpoints
- Updated nemo roles (internal role title mismatch)


v0.2.23
==

- Added instance_id filter query param in ```/workload/instance/```
- ```/workload/instance/``` now returns user details instead of user id 


v0.2.24
==

- Fix bug in workload template


v0.2.25
==

- Save user in workload template API view


v0.2.26
==

- Fix lifecycle worker

v0.2.27
==

- Fix lifecycle worker (deletion)

v0.2.28
==

- Deprecated cluster endpoints (to be removed in later version)


v0.2.29
==

- Added LOG_LEVEL env var support for django app log level handling
- Fix a bug in workload creation post processing steps

v0.2.30
==

- Minor swagger docs fix

v0.2.31
==

- Added linkerd injection in workloads