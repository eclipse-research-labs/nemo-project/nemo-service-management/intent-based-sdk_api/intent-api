FROM python:3.11.7-slim-bookworm as base

# Generate virtualenv in project directory
ENV PIPENV_VENV_IN_PROJECT=1
ENV PYTHONUNBUFFERED=1

# Project directory
ENV PROJECT_ROOT=/opt/app

WORKDIR $PROJECT_ROOT

# Add tools & runtime dependencies
RUN set -eux; \
        apt-get update; \
        apt-get install -y --no-install-recommends \
            netcat-traditional \
            vim \
            tmux \
            wget \
            curl \
            graphviz \
            postgresql-client \
            skopeo \
            git \
        ; \
        curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash; \
        curl -LO "https://dl.k8s.io/release/v1.29.1/bin/linux/amd64/kubectl"; \
        rm -rf /var/lib/apt/lists/*

# Copy Pipfile & lock
COPY Pipfile* $PROJECT_ROOT/

RUN mkdir $PROJECT_ROOT/wheels

COPY wheels/* $PROJECT_ROOT/wheels/

RUN set -eux; \
    savedAptMark="$(apt-mark showmanual)"; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libgraphviz-dev \
        build-essential \
        cmake \
        libffi-dev \
        libpq-dev \
    ; \
    pip3 install "pipenv==2023.10.3"; \
    pipenv install; \
	apt-mark auto '.*' > /dev/null; \
	apt-mark manual $savedAptMark; \
    find /opt/app/.venv -type f -executable -exec ldd '{}' ';' 2>&1 \
        | grep -v "not a dynamic executable" \
        | awk '/=>/ { so = $(NF-1); if (index(so, "/opt/app/.venv") == 1) { next }; gsub("^/(usr/)?", "", so); printf "*%s\n", so }' \
        | grep -v "*not" \
        | xargs -r dpkg-query --search 2>&1 \
        | grep -v "dpkg-query" \
        | cut -d: -f1 \
        | sort -u \
        | xargs -r apt-mark manual \
    ; \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false; \
	rm -rf /var/lib/apt/lists/*; \
    pipenv run pip3 freeze;

FROM base as image

# Copy the application code
COPY . .

# Setup
RUN set -eux; \
    pipenv run python3 manage.py collectstatic --noinput

# Gunicorn configuration
ENV GUNICORN_CMD_ARGS="-c core/gunicorn.py"

#CMD bash config/scripts/bootstrap.sh
RUN chmod +x /opt/app/config/scripts/bootstrap.sh
ENTRYPOINT ["/opt/app/config/scripts/bootstrap.sh"]

# Expose gunicorn port
EXPOSE 8000
